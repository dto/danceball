(in-package :danceball)

;;; Enemies

(defclass enemy () ())

(defmethod will-obstruct-p ((wall wall) (enemy enemy)) t)

(defmethod damage :after ((enemy enemy) &optional points)
  (make-sparks (+ (x enemy) (units 0.5))
               (+ (y enemy) (units 0.5))))

(defmethod die :before ((enemy enemy))
  (award-extra-points (dj) 1000)
  (percent-of-time (with-difficulty 12 6 3)
    (let ((bomb (make-instance 'bomb)))
      (add-node (arena) bomb (x enemy) (y enemy))
      (fuzz-to-grid bomb))))

(defclass robot-1 (enemy grid-actor)
  ((color :initform "gold")
   (dance-animation :initform :robot-1-dance)
   (damage-animation :initform :robot-1-damage)
   (death-animation :initform :robot-1-death)))

(defmethod think ((robot robot-1))
  (when (and (on-beat-p)
             (at-rest-p robot))
    (or (percent-of-time (with-difficulty* 2 4 6)
          (when (kick-playing-p)
            (slide-on-grid robot (random-choose *grid-directions*)))
          t)
        (when (> *ship-shoot-distance*
                 (distance-between robot (cursor robot)))
          (or (when (kick-playing-p)
                (percent-of-time (with-difficulty* 3 5 7)
                  (slide-on-grid robot (direction-to-cursor robot)) t))
              (when (or (snare-playing-p)
                        (percent-of-time 2 t))
                (percent-of-time (with-difficulty* 2 4 6)
                    (attack robot (direction-to-cursor robot)))))))))

(defmethod attack ((robot robot-1) direction)
  (unless (member direction *grid-directions*)
    (setf direction (random-choose *grid-directions*)))
  (let ((bullet (make-instance 'bullet :direction direction)))
    (play-sound robot (random-bip-sound))
    (insert bullet)
    (move-to-grid bullet (grid-x robot) (grid-y robot))))

(defmethod current-animation-frame :around ((robot robot-1))
  (or (call-next-method)
      (nth (mod (current-dance-frame-index) 8)
           (spritesheet-animation-frames *spritesheet* (getf *animations*
                                                             (slot-value robot 'dance-animation))))))

(defclass robot-2 (robot-1)
  ((color :initform "yellow")
   (dance-animation :initform :robot-2-dance)
   (damage-animation :initform :robot-2-damage)
   (death-animation :initform :robot-2-death)))

(defclass robot-3 (robot-2)
  ((color :initform "orange")
   (dance-animation :initform :robot-3-dance)
   (damage-animation :initform :robot-3-damage)
   (death-animation :initform :robot-3-death)))

(defclass biclops (robot-1)
  ((color :initform "hot pink")
   (dance-animation :initform :biclops-dance)
   (damage-animation :initform :biclops-damage)
   (death-animation :initform :biclops-death)))

(defclass humanoid (robot-1)
  ((color :initform "cyan")
   (dance-animation :initform :humanoid-dance)
   (damage-animation :initform :humanoid-damage)
   (death-animation :initform :humanoid-death)))

(defmethod collide ((this robot-1) (that robot-1))
  (damage this 1)
  (damage that 1)
  (repel this that)
  (repel that this))

(defmethod collide ((robot robot-1) (wall wall))
  (repel wall robot))

;;; Ghost

(defparameter *ghost-images* (image-set "ghost" 6))

(defun ghost-health-points ()
  (truncate (with-difficulty* 6 10 12)))

(defclass ghost (enemy grid-agent)
  ((image :initform (random-choose *ghost-images*))
   (maximum-health-points :initform (ghost-health-points))
   (attack-clock :initform 0 :accessor attack-clock)))

(defmethod begin-attack-animation ((ghost ghost))
  (setf (attack-clock ghost) (seconds->frames 1)))

(defmethod update :before ((ghost ghost))
  (when (plusp (attack-clock ghost))
    (decf (attack-clock ghost)))
  (percent-of-time 20 (setf (image ghost) (random-choose *ghost-images*))))

(defmethod electrify ((ghost ghost))
  (begin-attack-animation ghost))

(defmethod think ((ghost ghost))
  (if (<= (distance-to-cursor ghost)
          (+ 50 (attack-radius ghost)))
      (electrify ghost)
      (seek-and-slide ghost)))

(defmethod attack-radius ((ghost ghost))
  (with-difficulty 50 80 100))

(defmethod think :around ((ghost ghost))
  (when (on-beat-p)
    (call-next-method)))

(defmethod will-obstruct-p ((gate gate) (ghost ghost)) t)
(defmethod will-obstruct-p ((brick brick) (ghost ghost)) t)
(defmethod will-obstruct-p ((wall wall) (ghost ghost)) t)

(defmethod attack-animation-frame ((ghost ghost))
  (nth (mod (current-dance-frame-index) 8)
       (spritesheet-animation-frames *spritesheet* (getf *animations* :waveform-dance))))

(defmethod think :after ((ghost ghost))
  (if (<= (distance-to-cursor ghost)
          (+ 50 (attack-radius ghost)))
      (percent-of-time 20 (damage (player-1) (with-difficulty 10 15 20)))
      (when (is-on-screen-p ghost)
        (percent-of-time 20 (discard-path ghost)))))

(defmethod damage :after ((ghost ghost) &optional (points 1))
  (declare (ignore points))
  (discard-path ghost))

;;; Mines bounce around and must be avoided

(defclass mine (grid-actor)
  ((direction :initform (random-choose *grid-directions*))))

(defmethod current-animation-frame ((mine mine))
  (nth (mod (current-dance-frame-index) 8)
       (spritesheet-animation-frames *spritesheet* (getf *animations* :mine-dance))))

(defmethod update :before ((mine mine))
  (unless (sliding-p mine)
    (fuzz-to-grid mine))
  (setf (color mine) (random-choose '("orange" "red" "yellow"))))

(defmethod think ((mine mine))
  (when (and (not (sliding-p mine))
             (on-beat-p))
    (slide-on-grid mine (direction mine))))

(defmethod collide ((mine mine) (wall wall))
  (cancel-slide mine)
  (setf (direction mine) (opposite-direction (direction mine))))

(defmethod collide ((mine mine) (brick brick))
  (cancel-slide mine)
  (setf (direction mine) (random-choose *grid-directions*)))

(defmethod collide ((mine mine) (gate gate))
  (cancel-slide mine)
  (setf (direction mine) (random-choose *grid-directions*)))

;;; Party formations

(defclass grouped ()
  ((formation-p :initform nil :accessor formation-p :initarg :formation-p)
   (leader :initform nil :accessor leader :initarg :leader)
   (followers :initform nil :accessor followers :initarg :followers)))

(defmethod leader-p ((thing grouped))
  (and (formation-p thing)
       (followers thing)))

(defmethod follower-p ((thing grouped))
  (and (formation-p thing)
       (leader thing)))

(defmethod follow-leader ((follower grouped) leader)
  (setf (formation-p follower) t)
  (setf (leader follower) leader)
  (setf (followers follower) nil)
  (pushnew follower (followers leader) :test 'eq))

(defmethod lead-followers ((leader grouped) followers)
  (setf (formation-p leader) t)
  (dolist (f followers)
    (follow-leader f leader)))

(defmethod in-front-of ((thing grouped) &optional (distance 1))
  (with-slots (grid-x grid-y) thing
    (step-in-direction grid-x grid-y (facing thing) distance)))

(defmethod behind ((thing grouped) &optional (distance 1))
  (with-slots (grid-x grid-y) thing
    (step-in-direction grid-x grid-y (opposite-direction (facing thing)) distance)))

(defmethod far-in-front-of ((thing grouped)) (in-front-of thing 2))
(defmethod far-behind ((thing grouped)) (behind thing 2))

(defmethod flanking-left ((thing grouped) &optional (distance 1))
  (with-slots (grid-x grid-y) thing
    (step-in-direction grid-x grid-y (leftward (facing thing)) distance)))

(defmethod flanking-right ((thing grouped) &optional (distance 1))
  (with-slots (grid-x grid-y) thing
    (step-in-direction grid-x grid-y (rightward (facing thing)) distance)))

(defmethod flanking-behind-left ((thing grouped) &optional (distance 1))
  (with-slots (grid-x grid-y) thing
    (step-in-direction grid-x grid-y (xelf::left-turn (leftward (facing thing))) distance)))

(defmethod flanking-behind-right ((thing grouped) &optional (distance 1))
  (with-slots (grid-x grid-y) thing
    (step-in-direction grid-x grid-y (xelf::right-turn (rightward (facing thing))) distance)))

(defmethod flanking-far-left ((thing grouped)) (flanking-left thing 2))
(defmethod flanking-far-right ((thing grouped)) (flanking-right thing 2))
(defmethod flanking-behind-far-left ((thing grouped)) (flanking-behind-left thing 2))
(defmethod flanking-behind-far-right ((thing grouped)) (flanking-behind-right thing 2))

(defun grid-things-at (gx gy)
  (let (things)
    (do-nodes (node (current-buffer))
      (when (typep node (find-class 'grid-bound))
	(with-slots (grid-x grid-y) node
	  (when (and (= gx grid-x)
		     (= gy grid-y))
	    (push node things)))))
    things))

(defmethod things-in-front-of ((thing grouped) &optional (distance 1))
  (multiple-value-call #'grid-things-at (in-front-of thing distance)))

(defmethod facing-obstacle-p ((thing grouped))
  (multiple-value-bind (gx gy) (in-front-of thing)
    (some #'(lambda (object)
              (will-obstruct-p object thing))
          (grid-things-at gx gy))))

(defmethod can-advance-p ((thing grouped))
  (or (and (not (minusp (grid-x thing)))
	   (minusp (grid-y thing)))
      (and (not (facing-edge-p thing))
	   (not (facing-obstacle-p thing)))))

(defparameter *formation-positions*
  '(:in-front-of :behind :far-in-front-of :far-behind
    :flanking-left :flanking-right
    :flanking-far-left :flanking-far-right
    :flanking-behind-left :flanking-behind-right
    :flanking-behind-far-left :flanking-behind-far-right))

(defun formation-position-function (position)
  (assert (member position *formation-positions*))
  (symbol-function (intern (symbol-name position) (find-package :danceball))))

(defmethod formation-position ((thing grouped) position)
  (funcall (formation-position-function position) thing))

(defmethod join-formation ((thing grouped) leader position)
  (follow-leader thing leader)
  (multiple-value-call #'move-to-grid thing
    (formation-position thing position)))

(defmethod formation-members ((thing grouped))
  (when (formation-p thing)
    (if (followers thing)
	(cons thing (followers thing))
	(formation-members (leader thing)))))

(defmethod march ((thing grouped))
  (map nil #'slide-forward (formation-members thing)))

(defmethod break-formation ((thing grouped))
  (when (formation-p thing)
    (if (formation-follower-p thing)
	(progn
	  (break-formation (leader thing))
	  (setf (leader thing) nil)
	  (setf (formation-p thing) nil))
	(let ((f (followers thing)))
	  (setf (formation-p thing) nil)
	  (setf (followers thing) nil)
	  (mapc #'break-formation f)))))

(defparameter *formations*
  '((roblock-1 :flanking-right roblock-1 :flanking-left roblock-1 :far-behind roblock-2)
    (roblock-1 :behind roblock-2 :flanking-left roblock-1)
    (roblock-3 :behind roblock-5 :far-behind roblock-5 :flanking-behind-left roblock-5)
    (roblock-1 :flanking-behind-left roblock-1 :behind roblock-3 :far-behind roblock-2)
    (roblock-4 :flanking-behind-left roblock-4 :flanking-behind-right roblock-4)
    (roblock-2 :flanking-behind-left roblock-2 :flanking-right roblock-3 :flanking-left roblock-1)
    (roblock-2 :behind roblock-4 :flanking-right roblock-4 :flanking-left roblock-2)
    (roblock-5 :flanking-left roblock-5 :flanking-right roblock-5 :behind roblock-3 :far-behind roblock-3)))

(defun make-leader (class x y)
  (let ((leader (make-instance class)))
    (prog1 leader
      (move-to-grid leader x y))))

(defun make-follower (leader class pos)
  (let ((follower (make-instance class)))
    (prog1 follower
      (join-formation follower leader pos))))

(defun make-formation (formation x y)
  (destructuring-bind (leader-job &rest pairs)
      formation ;;(mapcar #'make-keyword formation)
    (let ((leader (make-leader leader-job x y))
	  (followers nil))
      (loop while pairs do
	(let ((position (pop pairs))
	      (job (pop pairs)))
	  (push (make-follower leader job position) followers)))
      (assert (not (null followers)))
      (lead-followers leader followers)
      (cons leader followers))))

(defmethod face :after ((thing grouped) direction)
  (when (leader-p thing)
    (dolist (f followers)
      (face f direction))))

(defclass roblock (grouped) ())

(defmethod think :around ((roblock roblock))
  (when (and (leader-p roblock)
             (on-beat-p))
    (percent-of-time 10 (march roblock))))

(defmethod collide :around ((this roblock) (that roblock)) nil)

(defmethod initialize-instance :after ((roblock roblock) &key)
  (paint roblock (random-choose (theme-colors))))

(defclass roblock-1 (robot-1 roblock) ())
(defclass roblock-2 (robot-2 roblock) ())
(defclass roblock-3 (robot-3 roblock) ())
(defclass roblock-4 (humanoid roblock) ())
(defclass roblock-5 (biclops roblock) ())

(defparameter *roblock-classes* '(roblock-1 roblock-2 roblock-3 roblock-4 roblock-5)) 


