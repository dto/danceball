(in-package :danceball)

;;; Menu stuff

(defvar *tree-depth* 0)

(defmacro deeper (&rest body)
  `(let ((*tree-depth* (1+ *tree-depth*)))
     ,@body))

(defparameter *depth-gray-slope* -5)
(defparameter *depth-gray-base* 50)

(defun depth-gray (depth)
  (xelf::percent-gray (+ *depth-gray-base* (* depth *depth-gray-slope*))))

(defparameter *terminal-font* "sideways")
(defparameter *terminal-small* "sans-mono-14")
(defparameter *terminal-bold* "sideways")
(defparameter *heading-font* "sans-mono-bold-16")
(defparameter *foreground* "white")
(defparameter *background* "gray20")
(defparameter *highlight* "yellow")
(defparameter *point-color* "gray50")
(defparameter *border* "black")
(defparameter *tab-width* 20)
(defparameter *delimiter* "/")
(defparameter *root-label* *delimiter*)

(defun terminal-font-height () (font-height *terminal-font*))
(defun heading-font-height () (font-height *heading-font*))
(defun no-label-string () "<no label>")

(defvar *leaf* nil)
(defun leaf () *leaf*)
(defun set-leaf (leaf) (setf *leaf* leaf)) 

(defclass entry (thing)
  ((label :initform "no label" :accessor label :initarg :label)
   (timer :initform 0 :accessor timer)
   (nodes :initform () :accessor nodes :initarg :nodes)
   (parent :initform nil :accessor parent :initarg :parent)
   (point :initform 0 :accessor point :initarg :point)
   (highlight-p :initform t :accessor highlight-p :initarg :highlight-p)
   (expanded-p :initform t :accessor expanded-p :initarg :expanded-p)
   (command :initform nil :accessor command :initarg :command)))

(defmethod find-all-nodes ((entry entry))
  (append (list entry)
	  (mapcan #'find-all-nodes (nodes entry))))

(defmethod find-visible-nodes ((entry entry))
  (append (list entry)
	  (when (expanded-p entry) 
	    (mapcan #'find-visible-nodes (nodes entry)))))

(defmethod find-node-by-position ((entry entry) n)
  (let ((nodes (find-visible-nodes entry)))
    (when (<= 0 n (1- (length nodes)))
      (nth n nodes))))

(defmethod root-position ((entry entry))
  (let ((nodes (find-visible-nodes (find-root (current-buffer)))))
    (position entry nodes :test 'eq)))

(defmethod absolute-position ((entry entry))
  (let ((nodes (find-all-nodes (find-root (current-buffer)))))
    (position entry nodes :test 'eq)))

(defmethod relative-position ((entry entry))
  (if (parent entry)
      (position entry (nodes (parent entry)) :test 'eq)
      0))

(defun find-node (n)
  (find-node-by-position (find-root (current-buffer)) n))

(defmethod find-label ((entry entry)) (label entry))

(defmethod point-min ((entry entry)) (progn 0))
(defmethod point-max ((entry entry)) (length (find-all-nodes entry)))

(defmethod at-beginning-p ((entry entry))
  (= (point entry) (point-min entry)))

(defmethod at-end-p ((entry entry))
  (= (point entry) (1- (point-max entry))))

(defmethod restrict-point ((entry entry))
  (setf (point entry)
	(min (1- (point-max entry))
	     (max (point-min entry)
		  (point entry)))))

(defmethod next-line ((entry entry))
  (incf (point entry))
  (restrict-point entry)
  (set-leaf (find-node (point entry))))

(defmethod previous-line ((entry entry))
  (decf (point entry))
  (restrict-point entry)
  (set-leaf (find-node (point entry))))

(defmethod expand ((entry entry))
  (setf (expanded-p entry) t))

(defmethod unexpand ((entry entry))
  (setf (expanded-p entry) nil))

(defmethod toggle-expanded ((entry entry))
  (if (expanded-p entry)
      (unexpand entry)
      (expand entry)))

(defmethod activate ((entry entry))
  (if (nodes entry)
      nil
      ;; nil (toggle-expanded entry)
      (evaluate entry)))

;; (defmethod activate :around ((entry entry))
;;   (unless (plusp (timer entry))
;;     (call-next-method)))

(defmethod evaluate ((entry entry))
  (when (fboundp (command entry))
    (funcall (symbol-function (command entry))
	     (find-root (current-buffer)))))

(defgeneric find-height (entry))
(defgeneric find-width (entry))

(defmethod headline-height ((entry entry)) 
  (font-height *terminal-font*))

(defmethod headline-width ((entry entry)) 
  (if (find-label entry)
      (font-text-width (find-label entry) *terminal-bold*)
      1))

(defmethod body-height ((entry entry))
  (if (expanded-p entry)
      (apply #'+ (mapcar #'find-height (nodes entry)))
      0))

(defmethod body-width ((entry entry))
  (if (expanded-p entry)
      (apply #'+ (mapcar #'find-width (nodes entry)))
      0))

(defmethod find-height ((entry entry))
  (+ (headline-height entry)
     (body-height entry)))

(defmethod find-width ((entry entry))
  (+ (headline-width entry)
     (body-width entry)))

(defvar *indentation* 0)

(defmethod indentation ((entry entry)) *indentation*)

(defmethod tab-width ((entry entry)) *tab-width*)

(defmacro with-indentation (form &body body)
  `(let ((*indentation* ,form)) ,@body))

(defmethod layout ((entry entry) &optional (x0 0) (y0 0))
  (with-slots (height width) entry
    (move-to entry x0 y0)
    (setf height (find-height entry))
    (setf width (find-width entry))
    (incf y0 (headline-height entry))
    (deeper 
     (with-indentation (depth-indentation *tree-depth*)
       (dolist (node (nodes entry))
	 (layout node (+ (indentation entry) x0) y0)
	 (incf y0 (find-height node)))))))

(defmethod node-at-point ((entry entry))
  (let ((root (find-root (current-buffer))))
    (when root
      (find-node (point root)))))

(defmethod change-leaf ((entry entry) leaf)
  (set-leaf leaf))

(defmethod current-leaf ((entry entry)) 
  (leaf))

(defmethod set-point ((entry entry) point)
  (setf (point entry) point)
  (change-leaf entry (find-node point)))
	    
(defun depth-indentation (depth) (* *tab-width* depth))

(defmethod draw-highlight ((entry entry))
  (with-slots (x y expanded-p nodes) entry
    (draw-string (find-label entry) x y :color *highlight* :font *terminal-bold*)
    (draw-box (+ x (indentation entry))
	      (+ y -1 (font-height *terminal-bold*))
	      *width*
	      2 
	      :color "red")))

(defmethod draw-indicator ((entry entry))
  (with-slots (x y) entry
    (draw-textured-rectangle-*
     (+ x (headline-width entry) (units 1))
     (+ y 5) 0 8 8 (find-texture "triangle.png") :vertex-color "cyan")))

(defmethod decorate-headline ((entry entry)) nil)
  ;; (with-slots (x y) entry
  ;;   (draw-box (+ x (indentation entry))
  ;;             (+ y -1 (font-height *terminal-bold*))
  ;;             *width*
  ;;             2 
  ;;             :color "gray60")))
       
(defun stringify (entry stream)
  (format stream "<~A//~A>" (string-upcase (find-label entry)) (class-name (class-of entry))))

(defmethod print-object ((entry entry) stream)
  (stringify entry stream))

(defmethod draw ((entry entry))
  (with-slots (x y width height expanded-p nodes) entry
    (draw-string (find-label entry) x y :color *foreground* :font *terminal-font*)
    (when (and nodes (not expanded-p))
      (draw-indicator entry))
    (when (and nodes expanded-p)
      (decorate-headline entry) 
      (deeper (mapc #'draw nodes)))))

(defmethod draw-sparkle ((entry entry))
  (with-slots (x y) entry
      (draw-string (find-label entry) x y :color (random-choose '("cyan" "yellow" "magenta")) :font *terminal-font*)))

(defun button (form &optional label)
  (make-instance 'entry 
		 :label (or label (pretty-string form)) 
		 :command form))

(defmethod add-parent ((child entry) (parent entry))
  (setf (parent child) parent))

(defun folder (name &rest nodes)
  (let ((entry (make-instance 'entry 
			      :label (concatenate 'string *delimiter* 
						  (pretty-string name))
			      :nodes nodes)))
    (prog1 entry
      (dolist (node nodes)
	(add-parent node entry)))))

;;; Displaying text boxes

(defclass scroll (entry) 
  ((text :initform nil :initarg :text :accessor text)))

(defmethod find-height ((scroll scroll))
  (+ 
   (headline-height scroll)
   (* (font-height *terminal-small*) (length (text scroll)))))

(defmethod find-width ((scroll scroll)) *width*)

(defmethod clean-string* (string)
  (if (zerop (length string))
      " "
      string))

(defmethod draw ((scroll scroll))
  (with-slots (x y text) scroll
    (let ((y0 (+ y (headline-height scroll))))
      (dolist (line text)
	(draw-string (clean-string* line) x y0 :color "white" :font *terminal-small*)
	(incf y0 (font-height *terminal-small*))))))
    
(defparameter *scroll-instructions* " |          Press the Spacebar (or Menu button) to return to the menu.")

(defun make-scroll (string &optional label)
  (make-instance 'scroll :text (split-string-on-lines string)))

;;; The terminal browser and root menus

(defclass terminal (xelf:buffer) ())

(defmethod leave-menu ((terminal terminal))
  (at-next-update (exit terminal)))

(defmethod initialize-instance :after ((terminal terminal) &key)
  (bind-event terminal '(:space) 'leave-menu)
  (bind-event terminal '(:raw-joystick 0 :button-down) 'leave-menu))

(defmethod draw :before ((terminal terminal))
  (xelf::project-window terminal))

(defclass point (thing)
  ((blink-phase :initform 0.0 :accessor blink-phase)))

(defclass terminal-link (entry)
  ((terminal :initform nil :initarg :terminal :accessor terminal)))

(defmethod activate ((link terminal-link))
  ;;(set-exit-sector (terminal link) (current-buffer))
  (visit (terminal link)))

(defun make-link (form terminal)
  (make-instance 'terminal-link :terminal terminal :label (pretty-string form)))

(defun make-terminal (root)
  (let ((terminal (make-instance 'terminal)))
    (add-node terminal root 0 0)
    terminal))

(defun find-local-nodes ()
  (when (cursor)
    (mapcan #'find-nodes (find-local-selection (cursor)))))

(defun find-sector-nodes () 
  (when (current-buffer)
    (slot-value (current-buffer) 'nodes)))

(defun make-root-nodes ()
  (append
   (list 
    (folder :options
            (make-instance 'mode-selector-entry)
            (make-instance 'difficulty-selector-entry)
            (apply #'folder :controller-configuration
                   (append (find-controller-nodes)
                           (list (button 'save-configuration))))
            ))
   (list
    (apply #'folder :songs (find-song-nodes)))))
   
   ;; (button 'preferences)
	    ;; (button 'save-progress)
	    ;; (button 'load-progress)

(defmethod reset-game (root)
  (begin-game))

(defmethod quit-game (sector)
  (quit))

(defparameter *joystick-menu-timeout* 20)

(defclass root (entry) ())

(defmethod update-point ((root root))
  (let ((pos (root-position (current-leaf root))))
    (cond 
      ;; node at point is no longer shown
      ((null pos)
       (when (parent (current-leaf root))
	 (set-point root 0)
	 (set-leaf (first (nodes root)))))
      ;; did the position change due to expand/unexpand?
      ((and (numberp pos) (not (= pos (point root))))
       (set-point root pos)))))

(defmethod update-leaf ((root root))
  (let ((leaf (current-leaf root)))
    (when leaf (update-timer leaf))))

(defmethod update :before ((root root))
  (update-timer root)
  (update-point root)
  (update-leaf root)
  (update (dj))
  (labels ((pressed (button)
             (button-recently-pressed-p button)))
    (when
        (and (not (timer-running-p root))
             (cond ((pressed :up) (joystick-up root) nil)
                   ((pressed :down) (joystick-down root) nil)
                   ((pressed :cross) (activate root) t)
                   ((pressed :circle) (activate root) t)))
      (initialize-timer root)))
  (layout root))
    ;; (if (button-pressed-p *keyboard-controller* :cross)
    ;;     (activate root)
    ;;     (let ((heading 
    ;;     	(when (left-analog-stick-pressed-p *player-1-joystick*)
    ;;     	  (left-analog-stick-heading *player-1-joystick*))))
    ;;       (when heading
    ;;         (case (heading-direction heading)
    ;;           (:up (joystick-up root))
    ;;           (:upleft (joystick-up root))
    ;;           (:upright (joystick-up root))
    ;;           (:down (joystick-down root))
    ;;           (:downleft (joystick-down root))
    ;;           (:downright (joystick-down root)))))))

(defmethod update-timer (thing)
  (with-slots (timer) thing
    (when (plusp timer)
      (decf timer))))

(defmethod initialize-timer (thing &optional (time *joystick-menu-timeout*))
  (setf (timer thing) time))

(defmethod timer-running-p (thing)
  (plusp (timer thing)))

(defmethod joystick-up ((root root))
  (initialize-timer root)
  (previous root))

(defmethod joystick-down ((root root))
  (initialize-timer root)
  (next root))
	
(defmethod find-root ((terminal terminal))
  (first (find-instances terminal 'root)))

(defun make-main-menu () 
  (setf *capturing-button* nil)
  (make-instance 'root :label "Welcome to Danceball! Use the arrow keys to move and SHIFT or ENTER to select." :expanded-p t :nodes (make-root-nodes)))

(defmethod exit ((root root))
  (initialize-timer root)
  (visit (exit-sector (current-buffer))))
  
(defmethod draw :around ((root root))
  (let ((*tree-depth* 0))
    (call-next-method)))

;; (defmethod draw :before ((root root))
;;   (draw-box 0 0 *width* *height* :color "gray20")
;;   (with-slots (x y width height) root
;;     (draw-box x y *width* height :color *background*)))

(defmethod draw :after ((root root))
  (when (highlight-p root)
    (let ((leaf (current-leaf root)))
      (if (absolute-position leaf)
	  (draw-highlight leaf)
	  (progn (set-leaf (find-node (point root)))
		 (draw-highlight (current-leaf root)))))))

(defmethod activate ((root root))
  (play-sample "select.wav")
  (initialize-timer root)
  (let ((leaf (current-leaf root)))
    (when (and leaf
	       (not (eq root leaf)))
      (activate leaf))))

;; (defmethod handle-event :after ((terminal terminal) event)
;;   (let ((root (find-root terminal)))
;;     (at-next-update 
;;       (initialize-timer root)
;;       (handle-event root event))))

(defmethod initialize-instance :after ((root root) &key) nil)
  ;; (bind-event root '(:w) 'previous)
  ;; (bind-event root '(:s) 'next)
  ;; (bind-event root '(:up) 'previous)
  ;; (bind-event root '(:kp8) 'previous)
  ;; (bind-event root '(:down) 'next)
  ;; (bind-event root '(:kp2) 'next)
  ;; (bind-event root '(:escape) 'leave-menu)
  ;; (bind-event root '(:left) 'go-up)
  ;; (bind-event root '(:kp4) 'go-up)
  ;; (bind-event root '(:right) 'activate)
  ;; (bind-event root '(:kp6) 'activate)
  ;;(bind-event root '(:raw-joystick 999 :button-down) 'activate)
  ;; (bind-event root '(:return) 'activate)
  ;; (bind-event root '(:kp-enter) 'activate)
  ;; (bind-event root '(:rshift) 'activate)
  ;; (bind-event root '(:lshift) 'activate))

(defmethod refresh ((terminal terminal) &optional nodes)
  (set-point (find-root terminal) 1))

(defclass main-menu (terminal) ())

(defmethod draw :before ((menu main-menu))
  (draw-textured-rectangle-* -1000 -1000 0 4000 4000 (find-texture "white.png")
;;                             :window-x 0 :window-y 0
                             :vertex-color "gray20"))

(defmethod visit :after ((menu main-menu))
  (let ((node (find-node 1)))
    (move-window-to menu
                    -100 (- (/ (y node) 2) (/ *gl-screen-height* 4)))))

(defmethod refresh ((main-menu main-menu) &optional nodes)
  (let ((root (find-root main-menu)))
    (setf (nodes root) (or nodes (make-root-nodes)))
    (set-point root 1)
    (expand (node-at-point root))))

(defmethod initialize-instance :after ((main-menu main-menu) &key)
  (let ((menu (make-main-menu)))
    (add-node main-menu menu 0 0)
    (change-leaf menu (first (nodes menu)))))
    
(defmethod update :after ((menu main-menu))
  (update (find-root menu))
  (update-voice)
  (follow-point menu))

(defmethod follow-point ((menu main-menu))
  (with-slots (x y) (node-at-point (find-root menu))
    (glide-window-to menu
                    -100 (- (/ y 2) (/ *gl-screen-height* 4)))))

(defmethod next ((root root))
  (play-sample "keyclick.wav")
  (next-line root))

(defmethod previous ((root root))
  (play-sample "keyclick.wav")
  (previous-line root))

(defmethod go-up ((root root))
  (let ((node (current-leaf root)))
    (when (parent node)
      (set-point root (root-position (parent node))))))

(defmethod leave-menu ((root root)) nil)
  ;;(at-next-update (exit root)))

(defmethod visit :before ((root root))
  (read-user-config)
  (read-songs)
  (setf *capturing-button* nil)
  (choose-controller-profile)
  (initialize-timer root)
  (update-point root))

(defmethod visit :after ((root root))
  (set-point root 1)
  (update-point root))

;;  (follow-point root))

(defclass song-entry (entry)
  ((song :initform nil :initarg :song :accessor song)))

(defmethod layout ((entry song-entry) &optional (x0 0) (y0 0))
  (with-slots (height width) entry
    (move-to entry x0 y0)
    (setf height (find-height entry))
    (setf width (find-width entry))))

(defmethod find-height ((entry song-entry))
  (headline-height entry))

(defmethod evaluate ((entry song-entry))
  (play-current-mode (dj) (song entry)))

(defmethod draw :before ((entry song-entry)) nil)
  ;; (with-slots (x y) entry
  ;;   (draw-textured-rectangle-* x y 0 *width* (height entry)
  ;;                              (find-texture "menubar.png")
  ;;                              :vertex-color "gray10")))


(defun-memo song-label (song)
    (:key #'identity :test 'equal)
  (let ((ogg (song-ogg song)))
    (format nil "~A - ~S - ~A BPM"
            (song-artist ogg)
            (song-title ogg)
            (song-bpm ogg))))
                  
(defun song-node (song)
  (make-instance 'song-entry :label (song-label song) :song song))

(defun find-song-nodes ()
  (sort (mapcar #'song-node *level-songs*)
        #'(lambda (a b)
            (string< (song-artist (song-ogg (slot-value a 'song)))
                     (song-artist (song-ogg (slot-value b 'song)))))))

(defclass button-entry (entry)
  ((button :initform nil :initarg :button)))

(defvar *capturing-button* nil)

(defmethod evaluate ((entry button-entry))
  (setf *capturing-button* (slot-value entry 'button)))

(defmethod evaluate :after ((entry button-entry))
  (teach-say (dj) (slot-value entry 'button)))

(defun button-node (button)
  (make-instance 'button-entry :button button :label (symbol-name button)))
 
(defun find-controller-nodes ()
  (mapcar #'button-node '(:up :down :left :right :cross :circle :left-bumper :right-bumper)))
  
(defmethod find-label ((entry button-entry))
  (let ((data (find-map *controller-1* (slot-value entry 'button))))
    (if data
        (destructuring-bind (type number)
            data
          (concatenate 'string (label entry) " --> "
                       (string-downcase (symbol-name type))
                       " "
                       (cond ((= *axis-0-negative* number)
                              "-0")
                             ((= *axis-0-positive* number)
                              "+0")
                             (t (let ((sign
                                        (if (eq 'cl-user::button type)
                                            ""
                                            (if (plusp number)
                                                "+"
                                                "-"))))
                                  (format nil "~A~d" sign (abs number)))))))
        (concatenate 'string (label entry) " --> none"))))

(defmethod save-configuration ((root root))
  (let ((file (xelf::find-project-file "danceball" "config.lisp")))
    (write-sexp-to-file file
                        `(setf danceball::*default-controller-profile*
                               '(:axis-map ,(axis-map *controller-1*)
                                 :button-map ,(button-map *controller-1*))))))
  
(defmethod handle-event :after ((root root) event)
  (if (and (joystick-event-p event)
           *capturing-button*)
      (let ((button *capturing-button*))
        (setf *capturing-button* nil)
        (update-controller-profile button event))
      (when (or (equal '((lshift)) event)
                (equal '((rshift)) event))
        (activate root))))

(defun update-controller-profile (button event)
  (with-slots (button-map axis-map) *controller-1*
    (ecase (first event)
      (:joystick (destructuring-bind (which button0 key) (rest event)
                   (setf (getf button-map button) button0)
                   (remf axis-map button)))
      (:joystick-axis (destructuring-bind (axis value key) (rest event)
                        (let ((value0
                                (if (zerop axis)
                                    (if (minusp value)
                                        *axis-0-negative*
                                        *axis-0-positive*)
                                    (if (minusp value)
                                        (- 0 axis)
                                        axis))))
                          (setf (getf axis-map button) value0)
                          (remf button-map button)))))))
       
(defmethod draw :after ((entry button-entry))
  (when (eq *capturing-button* (slot-value entry 'button))
    (draw-sparkle entry)))

;;; A button to select the play mode 

(defclass mode-selector-entry (entry) ())

(defmethod evaluate ((entry mode-selector-entry))
  (setf (current-mode (dj))
        (ecase (current-mode (dj))
          (:puzzle :challenge)
          (:challenge :match)
          (:match :puzzle))))

(defmethod evaluate :after ((entry mode-selector-entry))
  (say (dj)
       (ecase (current-mode (dj))
         (:puzzle :puzzle-mode)
         (:challenge :challenge-mode)
         (:match :match-mode))))

(defmethod find-label ((entry mode-selector-entry))
  (format nil "~A mode" (string-downcase (symbol-name (current-mode (dj))))))

;;; Difficulty selector 

(defclass difficulty-selector-entry (entry) ())

(defmethod evaluate ((entry difficulty-selector-entry))
  (change-difficulty (dj)))

(defmethod find-label ((entry difficulty-selector-entry))
  (format nil "~A difficulty" (case *difficulty*
                                (0 "easy")
                                (1 "medium")
                                (2 "hard"))))

(defmethod menu-p ((menu main-menu)) t)
