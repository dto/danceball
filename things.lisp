(in-package :danceball)

(defclass thing (node) ())

(defmethod is-on-screen-p ((thing thing))
  (bounding-box-contains 
   (multiple-value-list (scale-bounding-box (multiple-value-list (window-bounding-box (current-buffer))) 1.2))
   (multiple-value-list (bounding-box thing))))

(defmethod paint ((thing thing) color)
  (setf (color thing) color))

;; If you define (COLLIDE A B) and instead (COLLIDE B A) gets called,
;; we still want to invoke (COLLIDE A B).

(defmethod xelf:handle-collision ((u thing) (v thing))
  (collide u v)
  (collide v u))

;;; Pathfinding obstruction declaration

(defmethod will-obstruct-p ((this thing) (that thing)) nil)

;; DEPLOY is called on each object after ARENA generation.
;; This is used to allow PATHFINDING to allocate memory
;; before gameplay starts; see below.

(defmethod deploy ((thing thing)) nil)

;;; Playing sounds only within a certain distance of the player

(defparameter *audible-distance* 1024)

(defmethod audible-p ((thing thing))
  (>= *audible-distance* (distance-between thing (player-1))))

(defmethod play-sound ((thing thing) sound)
  (when (audible-p thing)
    (play-sample sound)))

;;; Finding the center of the Arena

(defun center-of-arena ()
  (values (/ (width (arena)) 2.0) (/ (height (arena)) 2.0)))

(defmethod heading-to-center ((thing thing))
  (multiple-value-bind (tx ty) (center-point thing)
    (multiple-value-bind (cx cy) (center-of-arena)
      (find-heading tx ty cx cy))))

;;; Restricting objects to the buffer boundaries

(defmethod knock-toward-center ((thing thing))
  (multiple-value-bind (gx gy) (center-point thing)
    (multiple-value-bind (cx cy) (center-point (current-buffer))
      (let ((jerk-distance (/ (distance cx cy gx gy) 16)))
	(with-slots (heading) thing
	  (setf heading (find-heading gx gy cx cy))
	  (move thing heading jerk-distance))))))

(defmethod restrict-to-buffer ((thing thing))
  (unless (bounding-box-contains (multiple-value-list (bounding-box (current-buffer)))
				 (multiple-value-list (bounding-box thing)))
    (knock-toward-center thing)))

;;; Finding the player or other target object

(defmethod cursor ((thing thing)) (player-1))

(defmethod distance-to-cursor ((thing thing))
  (multiple-value-bind (cx cy) (center-point thing)
    (multiple-value-bind (px py) (center-point (cursor thing))
      (distance cx cy px py))))

(defmethod heading-to-cursor ((thing thing))
  (multiple-value-bind (cx cy) (center-point thing)
    (multiple-value-bind (px py) (center-point (cursor thing))
      (find-heading cx cy px py))))

(defmethod direction-to-cursor ((thing thing))
  (or (heading-direction (heading-to-cursor thing)) :right))

;;; Heading and facing orientation

(defmethod update-heading ((thing thing))
  (with-slots (heading direction) thing
    (setf heading (direction-heading direction))))

(defmethod face ((thing thing) direction)
  (setf (direction thing) direction)
  (update-heading thing))

(defmethod facing ((thing thing))
  (slot-value thing 'direction))

(defmethod turn-around ((thing thing))
  (face thing (opposite-direction (direction thing))))

(defun leftward (direction) (xelf::left-turn (xelf::left-turn direction)))
(defun rightward (direction) (xelf::right-turn (xelf::right-turn direction)))

(defmethod turn-leftward ((thing thing))
  (face thing (leftward (direction thing))))

(defmethod turn-rightward ((thing thing))
  (face thing (rightward (direction thing))))

;;; Spritesheet based animation 

(defclass animating ()
  ((delay :initform 0)
   (animation-frames :initform nil :accessor animation-frames)
   (static-frame :initform nil :accessor static-frame :initarg :static-frame)))

(defmethod current-animation-frame ((thing animating))
  (when (animation-frames thing)
    (first (animation-frames thing))))

(defmethod default-animation-frame ((thing animating))
  (static-frame thing))

(defmethod begin-animation ((thing animating) frames)
  (with-slots (animation-frames delay) thing
    (setf animation-frames frames)
    (setf delay (frame-delay (first frames)))))

(defmethod update-animation ((thing animating))
  (with-slots (animation-frames delay) thing
    (when animation-frames
      (when (not (minusp delay))
	(decf delay))
      (when (minusp delay)
	(pop animation-frames)
	(when animation-frames 
	  (setf delay (frame-delay (first animation-frames))))))))

(defmethod end-animation ((thing animating))
  (setf (animation-frames thing) nil))
   
(defmethod update :before ((thing animating))
  (update-animation thing))

;;; Hit points, damage, and death

(defparameter *default-maximum-health* 3)

(defclass dying ()
  ((dead-p :initform nil :initarg :dead-p :accessor dead-p)
   (maximum-health :initform *default-maximum-health* :initarg :maximum-health :accessor maximum-health)
   (health-points :initform *default-maximum-health* :initarg :health-points :accessor health-points)))

(defmethod initialize-instance :after ((thing dying) &key)
  (setf (health-points thing)
        (maximum-health thing)))

(defmethod damage ((thing dying) &optional (points 1))
  (decf (health-points thing) points))

(defmethod die ((thing dying))
  (setf (dead-p thing) t)
  (destroy thing))
   
(defmethod update :around ((thing dying))
  (if (and (not (dead-p thing))
           (minusp (health-points thing)))
      (die thing)
      (call-next-method)))

;; don't keep damaging dead objects if references are still retained
(defmethod damage :around ((thing dying) &optional (points 1))
  (declare (ignore points))
  (when (not (dead-p thing))
    (call-next-method)))

;;; Sliding movement
   
(defclass sliding ()
  ((px :initform 0)
   (py :initform 0)
   (x0 :initform 0)
   (y0 :initform 0)
   (interpolation :initform :linear)
   (frames :initform 0)
   (slide-frames :initform *slide-frames*)
   (slide-timer :initform 0)))
   
(defmethod sliding-p ((thing sliding))
  (plusp (slot-value thing 'slide-timer)))

(defmethod update :before ((thing sliding))
  (update-slide thing))

(defmethod arrive ((thing sliding)) nil)

(defmethod cancel-slide ((thing sliding))
  (setf (slot-value thing 'slide-timer) 0))

(defun interpolate (px x a &optional (interpolation :linear))
  (let ((range (- px x)))
    (float 
     (+ px
	(ecase interpolation
	  (:linear (* a range))
	  (:sine (* (sin (* a (/ pi 2))) range)))))))

(defmethod slide-to ((thing sliding) x1 y1 &key (interpolation :linear) frames)
  (with-slots (x y px py x0 y0 interpolation slide-timer slide-frames) thing
    (setf px x py y)
    (setf x0 x1 y0 y1)
    (setf interpolation interpolation)
    (let ((timer (or frames slide-frames)))
      (setf slide-timer timer frames timer))))

(defmethod update-slide ((thing sliding))
  (with-slots (x0 y0 px py slide-frames slide-timer interpolation) thing
    (unless (zerop slide-timer)
      ;; scale to [0,1]
      (let ((a (/ (- slide-timer slide-frames) slide-frames)))
	(move-to thing
		 (interpolate px x0 a interpolation)
		 (interpolate py y0 a interpolation))
	(decf slide-timer)
        (when (zerop slide-timer)
          (arrive thing))))))

;;; Physics things require extra computation per frame

(defclass physical ()
  ((tx :initform 0.0) ;; x axis thrust
   (ty :initform 0.0) ;; y axis thrust
   ;; physics state 
   (dx :initform 0.0) ;; x axis speed 
   (dy :initform 0.0) ;; y axis speed
   (ddx :initform 0.0) ;; x axis acceleration
   (ddy :initform 0.0) ;; y axis acceleration
   ;; physics parameters
   (max-dx :initform 100)
   (max-dy :initform 100)
   (max-ddx :initform 4)
   (max-ddy :initform 4)))
  
(defparameter *dead-zone* 0.1 "Minimum speed (dx) to consider as motion.")

(defmethod max-speed ((thing physical)) (slot-value thing 'max-dx))
(defmethod max-acceleration ((thing physical)) (slot-value thing 'max-ddx))

(defun clamp (x bound)
  (max (- bound)
       (min x bound)))

(defun clamp0 (x bound)
  (let ((value (clamp x bound)))
    (if (< (abs value) *dead-zone*)
	0
	value)))

(defmethod at-rest-p ((thing physical))
  (with-slots (dx dy) thing
    (and (> *dead-zone* (abs dx))
	 (> *dead-zone* (abs dy)))))

(defmethod slow-p ((thing physical))
  (with-slots (dx dy) thing
    (and (> 1 (abs dx))
	 (> 1 (abs dy)))))

(defmethod reset-physics ((thing physical))
  (with-slots (dx dy ddx ddy) thing
    (setf dx 0 dy 0 ddx 0 ddy 0)))

(defmethod impel ((thing physical) &key speed heading)
  (with-slots (tx ty dx dy ddx ddy) thing
    (setf (slot-value thing 'heading) heading)
    ;;(setf (slot-value thing 'direction) (heading-direction heading))
    (setf ddx 0 ddy 0)
    (setf dx (* speed (cos heading)))
    (setf dy (* speed (sin heading)))))

(defmethod repel ((this physical) (that physical) &optional (speed 10))
  (impel that :speed speed :heading (heading-between this that)))

(defmethod impel :after ((thing physical) &key speed heading)
  (play-sound thing (random-choose *whack-sounds*)))

(defmethod movement-heading ((thing physical)) nil)

(defmethod update-heading ((thing physical))
  (with-slots (heading) thing
    (setf heading (or (movement-heading thing) heading))))

(defparameter *thrust* 0.52 "Base amount of acceleration (ddx)" )

(defmethod current-heading ((thing physical)) 
  (slot-value thing 'heading))

(defmethod thrust-x ((thing physical)) 
  (when (movement-heading thing) *thrust*))

(defmethod thrust-y ((thing physical)) 
  (when (movement-heading thing) *thrust*))

;; Now we feed the thrust input into the physics system. The slots TX and
;; TY are the amount of X-axis and Y-axis thrust. A point on the unit
;; circle is used to compute the X and Y thrust amounts. 

(defmethod update-thrust ((thing physical))
  (with-slots (tx ty) thing
    (let ((heading (current-heading thing))
	  (thrust-x (thrust-x thing))
	  (thrust-y (thrust-y thing)))
      (setf tx (if thrust-x (* thrust-x (cos heading)) nil))
      (setf ty (if thrust-y (* thrust-y (sin heading)) nil)))))

;;; Friction

;; The DECAY method is used in UPDATE-PHYSICS to attenuate movement in
;; the absence of thrust or other impulse. It is analogous to air or
;; fluid resistance caused by friction. DECAY is made into a method so
;; that the classes for ROBOT and BALL can have different resistances.

(defun decay-more (x)
  (let ((z (* 0.94 x)))
    z))

(defmethod decay ((thing physical) value)
  (decay-more value))

;;; Core physics definitions

;; This is where everything comes together: Thrust is applied to
;; acceleration, which is applied to speed, which becomes finally a new
;; position for the object.

(defmethod update-derivatives ((thing physical))
  (with-slots (x y dx dy ddx ddy tx ty
		 max-dx max-ddx max-dy max-ddy) thing
    (setf ddx (clamp (or tx (decay thing ddx))
		     (max-acceleration thing)))
    (setf dx (clamp (if tx (+ dx ddx) (decay thing dx))
		    (max-speed thing)))
    (setf ddy (clamp (or ty (decay thing ddy))
		     (max-acceleration thing)))
    (setf dy (clamp (if ty (+ dy ddy) (decay thing dy))
		    (max-speed thing)))))

(defmethod update-position ((thing physical))
  (with-slots (x y dx dy) thing
    (move-to thing 
	     (+ x dx)
	     (+ y dy))))

;;; Physics update hook

;; Now we add a :BEFORE method to make sure objects physics are
;; updated properly once per frame, just before the objects' primary
;; UPDATE method runs.

(defmethod update-physics ((thing physical))
  (update-heading thing)
  (update-thrust thing)
  (update-derivatives thing)
  (update-position thing))

(defmethod update :before ((thing physical))
  (update-physics thing))

;;; Pathfinding entities need more memory and CPU

;; Only inherit from this class if you want to allocate memory for a
;; path upon every deployment.

(defclass pathfinding ()
  ((path :initform nil :accessor path)
   (waypoints :initform nil :accessor waypoints)
   (goal-x :initform nil :accessor goal-x)
   (goal-y :initform nil :accessor goal-y)))

;; We use the arena grid as the pathfinding grid.

(defmethod install-path ((thing pathfinding))
  (setf (path thing)
        (xelf:create-path thing
                          :width (truncate (/ (width (arena)) (units 1)))
                          :height (truncate (/ (height (arena)) (units 1)))
                          :buffer (arena))))

(defmethod ensure-path ((thing pathfinding))
  (when (null (path thing))
    (install-path thing)))

(defmethod deploy :after ((thing pathfinding))
  (ensure-path thing))

;; Ensure path exists before other updates, even if allocating now is
;; suboptimal
(defmethod update :before ((thing pathfinding))
  (ensure-path thing))

(defmethod seek-to ((thing pathfinding) x y)
  (with-slots (waypoints path) thing
    (setf waypoints (find-path-waypoints path (x thing) (y thing) x y))))

(defmethod discard-path ((thing pathfinding))
  (setf (waypoints thing) nil))

;;; Integrating the systems into one basic ACTOR class

(defclass actor (thing animating sliding physical dying) 
  ((dance-animation :initform :robot-1-dance :initarg :dance-animation :accessor dance-animation)
   (damage-animation :initform :robot-1-damage :initarg :damage-animation :accessor damage-animation)
   (death-animation :initform :robot-1-death :initarg :death-animation :accessor death-animation)))

;;; Animating and damage

(defmethod damage :after ((thing actor) &optional (points 1))
  (declare (ignore points))
  (begin-animation thing
                   (spritesheet-animation-frames *spritesheet* (getf *animations* (damage-animation thing)))))

;;; Thinking and sliding

(defmethod think ((thing actor)) nil)

(defmethod update :before ((thing actor))
  (when (should-think-p thing)
    (think thing)))

(defmethod should-think-p ((thing actor))
  (not (sliding-p thing)))

;;; Don't update physics when sliding

(defmethod update-physics :around ((thing actor))
  (unless (sliding-p thing)
    (call-next-method)))

;;; Integrate buffer restriction and physics

(defmethod restrict-to-buffer :after ((thing actor))
  (reset-physics thing))
  
;;; Grid sizing and alignment

(defclass grid-bound ()
  ((grid-x :initform 0 :accessor grid-x :initarg :grid-x)
   (grid-y :initform 0 :accessor grid-y :initarg :grid-y)
   (origin-gx :initform 0)
   (origin-gy :initform 0)
   (gx0 :initform 0)
   (gy0 :initform 0)))

(defmethod resize-to-grid ((thing grid-bound))
  (resize thing
          (grid-object-size (current-buffer))
          (grid-object-size (current-buffer))))

(defmethod initialize-instance :after ((thing grid-bound) &key)
  (resize-to-grid thing))

(defmethod move-grid ((thing grid-bound) direction &optional (distance 1))
  (with-slots (grid-x grid-y) thing
    (multiple-value-bind (x y) 
	(step-in-direction grid-x grid-y direction distance)
      (move-to-grid thing x y))))

(defmethod move-forward ((thing grid-bound) &optional (distance 1))
  (with-slots (direction) thing
    (move-grid thing direction distance)))

(defmethod move-to-grid ((thing grid-bound) gx gy)
  (assert (and (integerp gx) (integerp gy)))
  (with-slots (height width grid-x grid-y gx0 gy0) thing
    (multiple-value-bind (x y) (grid-position gx gy)
      (setf grid-x gx grid-y gy)
      (move-to thing 
	       (+ x (grid-offset (current-buffer)))
	       (+ y (grid-offset (current-buffer)))
	       (setf gx0 gx gy0 gy)))))

(defmethod place ((arena arena) (thing grid-bound) gx gy)
  (with-buffer arena
    (add-node arena thing)
    (move-to-grid thing gx gy)))

(defmethod grid-coordinates ((thing grid-bound))
  (values (grid-x thing) (grid-y thing)))

(defmethod snap-to-grid ((thing grid-bound))
  (with-slots (gx0 gy0) thing
    (move-to-grid thing gx0 gy0)))

(defmethod fuzz-to-grid ((thing grid-bound))
  (with-slots (x y) thing
    (let ((tolerance 30)
	  (gx (round (/ x (units 1))))
	  (gy (round (/ y (units 1)))))
      (if (< tolerance (distance x y (* *unit* gx) (* *unit* gy)))
	  (message "Warning: could not fuzzy-match ~S for grid position ~S." thing (list gx gy 'from x y))
	  (move-to-grid thing gx gy)))))

;;; Grid actor

(defclass grid-actor (actor grid-bound)
  ((half-beat-shielded-p :initform nil
                         :accessor half-beat-shielded-p
                         :initarg :half-beat-shielded-p)))

;;; Integrate grid and sliding

(defmethod slide-to-grid ((thing grid-actor) gx gy &key (interpolation :linear) frames)
  (multiple-value-bind (cx cy) (grid-position-center gx gy)
    (with-slots (grid-x grid-y gx0 gy0 origin-gx origin-gy height width slide-frames) thing
      (setf gx0 gx gy0 gy)
      (setf origin-gx grid-x origin-gy grid-y) 
      (let ((timer (or frames slide-frames)))
	(slide-to thing (- cx (/ width 2)) (- cy (/ height 2)) :interpolation interpolation :frames timer)))))

(defmethod slide-on-grid ((thing grid-actor) direction &optional (distance 1) frames)
  (with-slots (grid-x grid-y slide-frames) thing
    (multiple-value-bind (x y) 
	(step-in-direction grid-x grid-y direction distance)
      (slide-to-grid thing x y :frames (or frames slide-frames)))))

(defmethod slide-forward ((thing grid-actor) &optional (distance 1) frames)
  (with-slots (direction slide-frames) thing
    (slide-on-grid thing direction distance slide-frames)))

(defmethod arrive :before ((thing grid-actor)) 
  (with-slots (gx0 gy0) thing
    (move-to-grid thing gx0 gy0)
    (snap-to-grid thing)))

(defmethod cancel-slide :after ((thing grid-actor))
  (with-slots (origin-gx origin-gy) thing
    (move-to-grid thing origin-gx origin-gy)
    (snap-to-grid thing)))

;;; Integrate sliding and physics with grid

(defmethod update-position :around ((thing grid-actor))
  (if (and (not (sliding-p thing))
           (at-rest-p thing))
      (fuzz-to-grid thing)
      (call-next-method)))

(defmethod fuzz-to-grid :after ((thing grid-actor))
  (reset-physics thing))

(defmethod current-shield-animation-frame ((thing grid-actor))
  (nth (mod (current-dance-frame-index) 8)
       (spritesheet-animation-frames *spritesheet* (getf *animations* :box-dance))))

;;; Agents

(defclass free-agent (pathfinding actor) ())
(defclass grid-agent (pathfinding grid-actor) ())

(defmethod seek-to-grid ((thing grid-agent) grid-x grid-y)
  (with-slots (waypoints path) thing
    (let ((x0 (* (units 1) (grid-x thing)))
          (y0 (* (units 1) (grid-y thing)))
          (x1 (* (units 1) grid-x))
          (y1 (* (units 1) grid-y)))
      (setf waypoints (find-path-waypoints path x0 y0 x1 y1)))))

(defmethod seek-and-slide ((thing grid-agent))
  (if (null (waypoints thing))
      (seek-to-grid thing
                    (grid-x (player-1))
                    (grid-y (player-1)))
      (destructuring-bind (x y) (pop (waypoints thing))
        (let ((gx (truncate (/ x (units 1))))
              (gy (truncate (/ y (units 1)))))
          (slide-to-grid thing gx gy)))))

;;; Playfield border walls

;; These are impenetrable, indestructible walls that surround the
;; playfield on all sides of the map. 

(defclass wall (thing physical)
  ((color :initform *wall-color*)))

(defun make-wall (x y width height)
  (let ((wall (make-instance 'wall)))
    (xelf:resize wall width height)
    (xelf:move-to wall x y)
    wall))

;;; Breakable bricks

(defparameter *brick-width* 24)
(defparameter *brick-height* 24)

(defclass brick (thing)
  ((collision-type :initform :passive)
   (color :initform "white")
   (image :initform "white.png")
   (height :initform *brick-height*)
   (width :initform *brick-width*)))
                                                         
(defmethod xelf:handle-collision ((this brick) (that brick)) nil)
(defmethod update :around ((brick brick)) nil)
           
;; Here we define some utility functions for placing groups of bricks
;; into a buffer. These will be combined into the final game board during
;; startup.

(defun make-brick (x y &optional (color "cyan"))
  (let ((brick (make-instance 'brick)))
    (move-to brick x y)
    (setf (slot-value brick 'color) color)
    brick))

(defun make-column (x y count &optional (color "cyan"))
  (with-new-buffer
    (dotimes (n count)
      (add-node (current-buffer) (make-brick x y color) x y)
      (incf y *brick-height*))
    (current-buffer)))

(defparameter *fortress-height* 28)

(defun make-fortress (x y colors)
  (with-new-buffer 
    (dolist (color colors)
      (paste-from (current-buffer) (make-column x y *fortress-height* color))
      (incf x *brick-width*)
      (current-buffer))))

;;; A border around the playfield

(defun make-border (x y width height)
  (let ((left x)
	(top y)
	(right (+ x width))
	(bottom (+ y height)))
    (with-new-buffer
      ;; top wall
      (insert (make-wall left top (- right left) (units 1)))
      ;; bottom wall
      (insert (make-wall left bottom (- right left (units -1)) (units 1)))
      ;; left wall
      (insert (make-wall left top (units 1) (- bottom top)))
      ;; right wall
      (insert (make-wall right top (units 1) (- bottom top (units -1))))
      ;; send it back
      (current-buffer))))

;;; The STAR is an obtainable item for puzzle mode

(defclass star (grid-actor) ())

(defmethod update ((star star))
  (paint star (random-choose '("red" "yellow" "magenta" "cyan"))))

(defmethod current-animation-frame ((star star))
  (nth (mod (current-dance-frame-index) 8)
       (spritesheet-animation-frames *spritesheet* (getf *animations* :star-dance))))

(defun find-star ()
  (let ((stars (find-instances (arena) 'star)))
    (when stars
      (first stars))))

(defmethod heading-to-star ((thing thing))
  (when (find-star)
    (heading-between thing (find-star))))

(defun star-indicator-position ()
  (step-toward-heading (player-1) (heading-to-star (player-1)) 300))

;;; Sparks and glows are non-colliding visual effects

(defclass spark (thing)
  ((clock :initform 20 :accessor clock :initarg :clock)
   (glow-p :initform nil :accessor glow-p)
   (direction :initform (xelf::random-direction))
   (collision-type :initform :passive)))

(defclass glow (thing)
  ((clock :initform 20 :accessor clock :initarg :clock)
   (collision-type :initform :passive)))

(defmethod update ((thing glow))
  (with-slots (clock) thing
    (when (plusp clock)
      (decf clock))
    (when (zerop clock)
      (destroy thing))))

(defparameter *rez-blurs* (image-set "rez-blur" 5))
(defparameter *rez-lights* (image-set "rez-light" 5))

(defmethod update ((thing spark))
  (move-toward thing (direction thing) (+ 2 (random 4)))
  (with-slots (clock glow-p) thing
    (when (plusp clock)
      (decf clock))
    (when (not glow-p)
      (setf glow-p t)
      (multiple-value-bind (cx cy) (center-point thing)
	(add-node (current-buffer) (make-instance 'glow) cx cy)))
    (when (zerop clock)
      (destroy thing))))

(defun make-sparks (x y &optional color)
  (dotimes (z 5)
    (add-node (current-buffer)
	      (make-instance 'spark)
	      (+ x -15 (random 30)) (+ y -15 (random 30)))))

;;; Dashed gates open with the correct color ball

(defclass gate (thing)
  ((image :initform "gate.png")
   (color :initform (random-choose '("red" "blue" "orange" "yellow" "green")))))

(defun make-gate (x y w h c)
  (let ((gate (make-instance 'gate)))
    (resize gate w h)
    (move-to gate x y)
    (paint gate c)
    gate))

;; See also ball.lisp for implementation of the mechanics.

;;; Hold arrows require the player to press and continue holding an arrow

(defclass arrow (grid-actor)
  ((direction :initform :right)
   (was-activated :initform nil)
   (hurt-player-p :initform nil :accessor hurt-player-p)
   (hold-length :initform 2 :initarg :hold-length :accessor hold-length)))

(defmethod update ((arrow arrow))
  (when (and (on-beat-p)
             (activated-p arrow)
             (not (button-recently-pressed-p (opposite-direction (direction arrow))))
             (not (button-held-p (opposite-direction (direction arrow)))))
    (damage (player-1) 3)
    (setf (hurt-player-p arrow) t)))

(defmethod update :after ((arrow arrow))
  (if (is-on-screen-p arrow)
      (progn
        (setf (color arrow) (random-choose '("magenta" "yellow" "green")))
        (when (and (slot-value arrow 'was-activated)
                   (not (activated-p arrow)))
          (destroy arrow)))
      (destroy arrow)))

(defmethod destroy :before ((arrow arrow))
  (unless (hurt-player-p arrow)
    (award-extra-points (dj) 5000)))

(defmethod activated-p ((arrow arrow))
  (with-slots (direction hold-length) arrow
    (multiple-value-bind (px py) (grid-coordinates (player-1))
      (multiple-value-bind (ax ay) (grid-coordinates arrow)
        (ecase direction
          (:up    (<= ay py (+ ay -1 hold-length)))
          (:down  (<= (- ay -1 hold-length) py ay))
          (:left  (<= ax px (+ ax -1 hold-length)))
          (:right (<= (- ax -1 hold-length) px ax)))))))

(defmethod activated-p :around ((arrow arrow))
  (setf (slot-value arrow 'was-activated)
        (call-next-method)))

(defmethod current-animation-frame ((arrow arrow))
  (nth (mod (current-dance-frame-index) 4)
       (spritesheet-animation-frames *spritesheet* (getf *animations* (opposite-direction (direction arrow))))))

(defmethod place-arrow ((arena arena) direction hold-length distance)
  (multiple-value-bind (gx gy)
      (step-in-direction (grid-x (player-1))
                         (grid-y (player-1))
                         (opposite-direction direction)
                         distance)
    (let ((arrow (make-instance 'arrow :direction direction :hold-length hold-length)))
      (add-node arena arrow)
      (move-to-grid arrow gx gy))))

(defmethod think ((arrow arrow))
  (when (and (not (sliding-p arrow))
             (on-beat-p))
    (slide-on-grid arrow (direction arrow))))

(defmethod collide :around ((arrow arrow) (thing thing)) nil)
(defmethod restrict-to-buffer :around ((arrow arrow)) nil)

;;; Chevrons help the player get around the level and escape difficult situations

(defclass chevron (thing)
  ((image :initform "chevrons.png")))

(defparameter *chevron-short-side* (units 1))
(defparameter *chevron-long-side* (units 12))

(defmethod initialize-instance :after ((chevron chevron) &key)
  (setf (heading chevron) (direction-heading (direction chevron)))
  (ecase (direction chevron)
    (:up
     (resize chevron *chevron-short-side* *chevron-long-side*)
     (setf (image chevron) "chevron-up.png"))
    (:down
     (resize chevron *chevron-short-side* *chevron-long-side*)
     (setf (image chevron) "chevron-down.png"))
    (:left
     (resize chevron *chevron-long-side* *chevron-short-side*)
     (setf (image chevron) "chevron-left.png"))
    (:right
     (resize chevron *chevron-long-side* *chevron-short-side*)
     (setf (image chevron) "chevron-right.png"))))

(defmethod update :before ((chevron chevron))
  (setf (color chevron) (random-choose '("white" "cyan" "yellow"))))

;; for level generation, we always generate them in pairs for bidirectional travel
(defun chevrons (&optional direction)
  (let ((dir (or direction (random-choose *grid-directions*))))
    (ecase dir
      ((:up :down) (bordered  (lined-up (singleton (make-instance 'chevron :direction dir))
                               (singleton (make-instance 'chevron :direction (opposite-direction dir))))))
      ((:left :right) (bordered (stacked-up (singleton (make-instance 'chevron :direction dir))
                                                          (singleton (make-instance 'chevron :direction (opposite-direction dir)))))))))

;;; Bombs can be triggered by the player to destroy robotic enemies

(defclass bomb (grid-actor) ())

(defmethod update :before ((bomb bomb))
  (setf (color bomb) (random-choose '("cyan" "yellow"))))

(defmethod current-animation-frame ((bomb bomb))
  (nth (mod (current-dance-frame-index) 8)
       (spritesheet-animation-frames *spritesheet* (getf *animations* :bomb-dance))))

;; See also player.lisp for implementation details.

;;; Enemy bullets move on the beat

(defclass bullet (grid-actor)
  ((static-frame :initform (single-frame *spritesheet* 14 8))))

(defmethod current-animation-frame ((bullet bullet))
  (let ((key (case (direction bullet)
               (:up :bullet-dance-up)
               (:down :bullet-dance-down)
               (:left :bullet-dance-left)
               (:right :bullet-dance-right))))
    (let ((frames (spritesheet-animation-frames
                   *spritesheet*
                   (getf *animations* key))))
      (nth (mod (current-dance-frame-index) 8) frames))))

(defun bullet-color ()
  (if (on-beat-p)
      (random-choose '("cyan" "yellow"))
      (random-choose '("magenta" "hot pink"))))

(defmethod update :before ((bullet bullet))
  (setf (color bullet) (bullet-color)))

(defmethod think ((bullet bullet))
  (when (and (not (sliding-p bullet))
             (on-beat-p))
    (slide-on-grid bullet (direction bullet) 1)))

(defmethod collide ((bullet bullet) (wall wall))
  (destroy bullet))

(defmethod collide ((bullet bullet) (gate gate))
  (destroy bullet))

(defmethod arrive :around ((bullet bullet)) nil)

;;; Spawners create robots

(defclass spawner (grid-actor)
  ((clock :initform (* 60.0
                       (with-difficulty* 4.0 2.0 1.0)
                       (seconds-per-beat)))))

(defmethod update :before ((spawner spawner))
  (with-slots (color) spawner
    (setf color (random-choose '("cyan" "magenta" "yellow"))))
  (with-slots (clock) spawner
    (if (plusp clock)
        (decf clock)
        (when (on-beat-p)
          (spawn spawner)
          (destroy spawner)))))

(defmethod spawn ((spawner spawner))
  (let ((enemy (make-instance (random-choose '(robot-1 robot-2 robot-3 biclops humanoid)))))
    (add-node (current-buffer) enemy (x spawner) (y spawner))
    (percent-of-time 14
      (setf (half-beat-shielded-p enemy) t))
    (fuzz-to-grid enemy)))

(defmethod current-animation-frame ((spawner spawner))
  (nth (mod (current-dance-frame-index) 8)
       (spritesheet-animation-frames *spritesheet* (getf *animations* :waveform-dance))))

