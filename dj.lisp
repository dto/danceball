(in-package :danceball)

(defparameter *voice-data*
'((:snakes "Avoid the snakes and their trails!" "snakes.wav")
 (:holds "Hold the indicated arrows!" "holds.wav")
 (:ghosts "Destroy the ghosts!" "ghosts.wav")
 (:robots "Destroy the robots!" "robots.wav")
 (:mines "Avoid the mines!" "mines.wav")
 (:versus-mode "Versus play." "versus-mode.wav")
 (:match-mode "Match three play." "match-mode.wav")
 (:challenge-mode "Challenge play." "challenge-mode.wav")
 (:puzzle-mode "Puzzle play." "puzzle-mode.wav")
 (:basic-help-8 "Follow the sparkling black and white point to find the star." "basic-help-8.wav")
 (:basic-help-7 "Get to the Star and survive the song!" "basic-help-7.wav")
 (:basic-help-6 "When arrows sweep across the screen, hold the opposite direction until it passes!" "basic-help-6.wav")
 (:basic-help-5 "Use shields to defend against bullets." "basic-help-5.wav")
 (:basic-help-4 "Break through dashed barriers with the correct color ball." "basic-help-4.wav")
 (:basic-help-3 "Use the Dance ball to break colored bricks and pick up their colors." "basic-help-3.wav")
 (:basic-help-2 "Press cross or circle (or the shift key)
 to fire the bouncing Dance ball!" "basic-help-2.wav")
 (:basic-help-1 "Press the buttons on the beat or the half-beat to move your triangle!" "basic-help-1.wav")
 (:finished "We're finished. Your controller is ready to use." "finished.wav")
 (:you-win "You win!" "you-win.wav")
 (:failed "You failed!" "failed.wav")
 (:to-start-over "If you want to start over, press the escape key." "to-start-over.wav")
 (:right-bumper "Press the right shoulder button." "right-bumper.wav")
 (:left-bumper "Press the left shoulder button." "left-bumper.wav")
 (:circle "Press the circle button." "circle.wav")
 (:cross "Press the cross button." "cross.wav")
 (:right "Press the right button." "right.wav")
 (:left "Press the left button." "left.wav")
 (:down "Press the down button." "down.wav")
 (:up "Press the up button." "up.wav")
 (:now-press-in-order "Now let's press each button in order." "now-press-in-order.wav")
 (:save-your-configuration "Choose the save option from the menu to save your configuration." "save-your-configuration.wav")
 (:beats-per-minute "Beats per minute" "beats-per-minute.wav")
 (:9 "Nine." "9.wav")
 (:8 "Eight." "8.wav")
 (:7 "Seven." "7.wav")
 (:6 "Six." "6.wav")
 (:5 "Five." "5.wav")
 (:4 "Four." "4.wav")
 (:3 "Three." "3.wav")
 (:2 "Two." "2.wav")
 (:1 "One." "1.wav")
 (:0 "Zero." "0.wav")
 (:hard "Hard difficulty." "hard.wav")
 (:medium "Medium difficulty." "medium.wav")
 (:easy "Easy difficulty." "easy.wav")
 (:player-2 "Player two." "player-2.wav")
 (:player-1 "Player one." "player-1.wav")
 (:press-any-button "To begin, press any button on the controller you want to use." "press-any-button.wav")
 (:now-lets-configure "Now let's configure your controller." "now-lets-configure.wav")
 (:now-press "Now press the controller button you wish to use for this command." "now-press.wav")
 (:please-connect "Please connect a game controller or dance pad to continue." "please-connect.wav")
 (:now-dance "Now, dance!" "now-dance.wav")
 (:got-star "You got the star!" "got-star.wav")
 (:now-survive "Now, survive the song!" "now-survive.wav")
 (:get-ready "Are you ready?" "get-ready.wav")
 (:keep-moving "Keep moving!" "keep-moving.wav")
 (:not-good "This doesn't look good!" "not-good.wav")
 (:ouch "Ouch!" "ouch.wav")
 (:good-show "Good show!" "good-show.wav")
 (:amazing "Amazing work!" "amazing.wav")
 (:excellent "Excellent!" "excellent.wav")
 (:marvelous "Marvelous!" "marvelous.wav")
 (:good "Good!" "good.wav")
 (:describe-escape-key "Press the escape key to go back." "describe-escape-key.wav")
 (:describe-enter-key "Use the enter or shift keys to make a selection." "describe-enter-key.wav")
 (:describe-arrow-keys "Use the arrow keys to move." "describe-arrow-keys.wav")
 (:welcome "Welcome to dance ball!" "welcome.wav")))
;; (eval-when (:compile-toplevel :execute)
;;                            (first (read-sexp-from-file
;;                                    (xelf::find-project-file "danceball" "screenplay.sexp")))))

(defun voice-data (key) (find key *voice-data* :key #'first))
(defun voice-text (key) (second (voice-data key)))
(defun voice-sample (key) (concatenate 'string "_" (third (voice-data key))))

(defparameter *voice-keys* (mapcar #'first *voice-data*))
(defvar *voice-key* nil)
(defvar *voice-channel* nil)
(defvar *voice-queue* nil)

(defun voice-speaking-p () *voice-channel*)
(defun voice-silent-p () (null *voice-channel*))

(defun voice-reset ()
  (when *voice-channel*
    (halt-sample *voice-channel*))
  (setf *voice-channel* nil)
  (setf *voice-queue* nil))

(defun voice-say (key)
  (setf *voice-key* key)
  (setf *voice-channel* 
	(play-sample (voice-sample key))))

(defun voice-queue (key)
  (when (not (or (eq key (first *voice-queue*))
		 (eq key *voice-key*)))
    (setf *voice-queue* (append *voice-queue* (list key)))))

(defun voice-unqueue ()
  (when *voice-queue*
    (pop *voice-queue*)))

(defun voice-callback (sample)
  (when (and (voice-speaking-p)
	     (= sample *voice-channel*))
    (setf *voice-channel* nil)
    (setf *voice-key* nil)
    (let ((next (voice-unqueue)))
      (when next (say (dj) next)))))

(defun update-voice ()
  (when (and (voice-speaking-p)
	     (null (sdl-mixer:sample-playing-p *voice-channel*)))
    (voice-callback *voice-channel*)))

(defun voice-error (key)
  (say (dj) key))

;;; DJ game manager wrapper class

(defvar *dj-use-voice-p* t)
(defun dj-use-voice-p () *dj-use-voice-p*)
(defun set-dj-use-voice-p (value) (setf *dj-use-voice-p* value))
(defsetf dj-use-voice-p set-dj-use-voice-p)

(defvar *dj-use-tutorial-p* t)
(defun dj-use-tutorial-p () *dj-use-tutorial-p*)
(defun set-dj-use-tutorial-p (value) (setf *dj-use-tutorial-p* value))
(defsetf dj-use-tutorial-p set-dj-use-tutorial-p)

(defparameter *dj-start-timer* (seconds->frames 3))

(defvar *dj* nil)
(defun dj () *dj*)
(defun set-dj (dj) (setf *dj* dj))
(defsetf dj set-dj)

(defclass dj ()
  ((current-song :initform nil :initarg :current-song :accessor current-song)
   (current-mode :initform :puzzle :initarg :current-mode :accessor current-mode)
   (taught-keys :initform nil :accessor taught-keys)
   (start-timer :initform 0 :initarg :start-timer :accessor start-timer)
   (num-steps :initform 0 :initarg :num-steps :accessor num-steps)
   (num-jumps :initform 0 :initarg :num-jumps :accessor num-jumps)
   (num-kicks :initform 0 :initarg :num-kicks :accessor num-kicks)
   (num-damage-points :initform 0 :initarg :num-damage-points :accessor num-damage-points)
   (num-stars :initform 0 :initarg :num-stars :accessor num-stars)
   (num-extra-points :initform 0 :initarg :num-extra-points :accessor num-extra-points)))

(defmethod initialize-score-data ((dj dj))
  (setf (num-steps dj) 0
        (num-jumps dj) 0
        (num-kicks dj) 0
        (num-damage-points dj) 0
        (num-stars dj) 0
        (num-extra-points dj) 0))

(defmethod compute-score ((dj dj) (arena arena))
  (declare (ignore arena))
  (truncate
   (+ (num-extra-points dj)
      (- (* 100 (+ (num-steps dj)
                   (num-jumps dj)
                   (num-kicks dj)
                   (* 25 (num-stars dj))))
         (* 10 (num-damage-points dj))))))

(defmethod award-extra-points ((dj dj) points)
  (incf (num-extra-points dj) points))

(defmethod update ((dj dj))
  (with-slots (start-timer) dj
    (when (plusp start-timer)
      (decf start-timer)))
  (update-elapsed-time)
  (update-beat)
  (update-song-clock)
  (update-voice)
  (update-button-states)
  (update-message-timer))

(defmethod update :before ((arena arena))
  (update (dj)))

(defmethod suspend-input ((dj dj) &optional duration-frames)
  (initialize-button-states)
  (setf (start-timer dj) (or duration-frames *dj-start-timer*)))

(defmethod resume-input ((dj dj))
  (initialize-button-states)
  (setf (start-timer dj) 0))

(defmethod input-suspended-p ((dj dj)) (plusp (start-timer dj)))

(defmethod initialize-instance :after ((dj dj) &key)
  (setf (dj) dj)
  (voice-reset)
  (setf (dj-use-tutorial-p) nil)
  (load-configuration dj))

(defmethod say ((dj dj) key &optional force)
  (if (or force (voice-silent-p))
      (progn (when force (voice-reset))
	     (voice-say key))
      (voice-queue key)))

(defmethod say :before ((dj dj) key &optional force)
  (notify dj (voice-text key)))

(defmethod say :around ((dj dj) key &optional force)
  (when (dj-use-voice-p)
    (call-next-method)))

(defmethod teach-say ((dj dj) key)
  (when (and *dj-use-tutorial-p*
             (not (member key (taught-keys dj))))
    (push key (taught-keys dj))
    (say dj key)))

(defmethod change-song ((dj dj) song)
  (setf (current-song dj) song)
  ;; preload music
  (find-resource (song-ogg song)))

(defmethod start-song ((dj dj) name)
  (let ((ogg (concatenate 'string name ".ogg"))
        (dat (concatenate 'string name ".dat")))
    (initialize-beat (song-bpm ogg))
    (load-beat-properties (xelf::find-project-file "danceball" dat))
    (setf *current-song-length* (* (seconds-per-beat)
                                   (length *beat-properties*)))
    (reset-song-clock)
    (play-music ogg)))

(defmethod play-song ((dj dj) &optional song)
  (suspend-input dj)
  (change-song dj (or song (current-song dj)))
  (start-song dj (current-song dj)))

(defmethod bpm ((dj dj) &optional song) (song-bpm (or song (current-song dj))))
(defmethod artist ((dj dj) &optional song) (song-artist (or song (current-song dj))))
(defmethod title ((dj dj) &optional song) (song-title (or song (current-song dj))))

(defmethod notify ((dj dj) string) (display-message string))

(defmethod change-difficulty ((dj dj) &optional difficulty)
  (setf *difficulty* (or difficulty (mod (1+ *difficulty*) 3))))

(defmethod change-difficulty :after ((dj dj) &optional difficulty)
  (declare (ignore difficulty))
  (say dj (ecase *difficulty*
            (0 :easy)
            (1 :medium)
            (2 :hard))))

(defmethod time-remaining ((dj dj)) *song-clock*)

(defmethod frames-remaining ((dj dj)) (seconds->frames (time-remaining dj)))

(defmethod find-players ((dj dj))
  (find-instances (arena) 'ship))

(defmethod load-configuration ((dj dj))
  (read-user-config)
  (read-songs)
  (choose-controller-profile)
  (index-pending-resources)
  (setf *keyboard-controller* (make-instance 'keyboard-controller))
  (initialize-button-states))

(defmethod replace-buffer ((dj dj) buffer)
  (let ((previous-buffer (current-buffer)))
    (at-next-update
      (when previous-buffer
        (destroy previous-buffer)
        (collect-garbage))
      (switch-to-buffer buffer)
      (visit buffer))))
;;(xelf::install-shell-keybindings buffer)))
    
(defmethod replace-buffer :after ((dj dj) buffer)
  (declare (ignore buffer))
  (initialize-score-data dj))

(defmethod show-menu ((dj dj) &optional (song (random-choose *demo-songs*)))
  (replace-buffer dj (make-instance 'main-menu))
  (at-next-update (play-song dj song)))

(defmethod show-menu :after ((dj dj) &optional song)
  (say dj :welcome))

(defmethod play-puzzle-mode ((dj dj) song)
  (replace-buffer dj (make-instance 'puzzle-arena))
  (at-next-update (play-song dj song)))
  
(defmethod play-puzzle-mode :after ((dj dj) song)
  (say dj :puzzle-mode)
  (say dj :get-ready)
  (say dj :now-dance))

(defmethod play-challenge-mode ((dj dj) song)
  (replace-buffer dj (make-instance 'challenge-arena))
  (at-next-update (play-song dj song)))

(defmethod play-challenge-mode :after ((dj dj) song)
  (say dj :challenge-mode)
  (say dj :get-ready)
  (say dj :now-dance))

(defmethod play-match-mode ((dj dj) song)
  (replace-buffer dj (make-instance 'match-arena))
  (at-next-update (play-song dj song)))
  
(defmethod play-match-mode :after ((dj dj) song)
  (say dj :match-mode)
  (say dj :get-ready)
  (say dj :now-dance))

;;; These AFTER methods on the player trigger score changes.

(defmethod slide-on-grid :after ((ship ship) direction &optional distance frames)
  (incf (num-steps (dj))))

(defmethod raise-shield :after ((ship ship) direction)
  (incf (num-jumps (dj))))

(defmethod collide :after ((ship ship) (star star))
  (incf (num-stars (dj)))
  (say (dj) (random-choose '(:good :marvelous :excellent :amazing :good-show))))

(defmethod kick :after ((ship ship))
  (incf (num-kicks (dj))))

(defmethod damage :after ((ship ship) &optional points)
  (incf (num-damage-points (dj)) points)
  (when (>= 30 (health-points ship))
    (percent-of-time 25 (say (dj) (random-choose '(:ouch :not-good :keep-moving)))))
  (play-sample (random-choose '("damage-1.wav" "damage-2.wav" "damage-3.wav"))))

;;; Nothing should move or spawn or damage while input is suspended
;;; (or song has ended).

(defmethod should-think-p :around ((thing thing))
  (unless (or (input-suspended-p (dj))
              (song-ended-p (arena)))
    (call-next-method)))

(defmethod add-spawner :around ((arena arena))
  (unless (or (input-suspended-p (dj))
              (song-ended-p (arena)))
    (call-next-method)))

(defmethod place-arrow :around ((arena arena) direction hold-length distance)
  (declare (ignore direction hold-length distance))
  (unless (or (input-suspended-p (dj))
              (song-ended-p (arena)))
    (call-next-method)))

(defmethod damage :around ((thing thing) &optional (points 1))
  (declare (ignore points))
  (unless (or (input-suspended-p (dj))
              (song-ended-p (arena)))
    (call-next-method)))

;;; Failing and clearing arenas

(defmethod fail ((dj dj))
  (halt-music)
  (show-card (arena) "failed.png"))

(defmethod fail :after ((dj dj))
  (say (dj) :failed))

(defmethod clear ((dj dj))
  (suspend-input dj)
  (show-card (arena) "cleared.png"))

(defmethod clear :after ((dj dj))
  (play-sample "megahit.wav")
  (play-sample "applause.wav")
  (say (dj) :you-win))

(defmethod clear :around ((dj dj))
  (when (not (dead-p (player-1)))
    (call-next-method)))

;;; Choosing buffers based on the current mode

(defmethod play-current-mode ((dj dj) song)
  (ecase (current-mode dj)
    (:puzzle (play-puzzle-mode dj song))
    (:challenge (play-challenge-mode dj song))
    (:match (play-match-mode dj song))))

;;; Tutorial triggers

(defmethod deploy :after ((ship ship))
  (teach-say (dj) :basic-help-1)
  (teach-say (dj) :basic-help-2))

(defmethod kick :after ((ship ship))
  (teach-say (dj) :basic-help-3)
  (teach-say (dj) :basic-help-4))

(defmethod open-gate ((gate gate))
  (teach-say (dj) :basic-help-7)
  (teach-say (dj) :basic-help-8))

(defmethod attack :after (robot direction)
  (teach-say (dj) :basic-help-5))

(defmethod place-arrow :after ((arena arena) direction hold-length distance)
  (teach-say (dj) :basic-help-6))

(defmethod visit :before ((menu main-menu))
  (teach-say (dj) :describe-arrow-keys)
  (teach-say (dj) :describe-enter-key)
  ;; eliminate any reference to an arena
  (setf *arena* nil))

