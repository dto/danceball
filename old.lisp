;;; Networking support

(defmethod find-identifier ((thing thing))
  (xelf:make-keyword (xelf:uuid thing)))

(defun find-thing-from-id (id)
  (xelf:find-object (symbol-name id) t))

(setf *identifier-search-function* 
      #'find-thing-from-id)

(setf xelf:*game-variables* 
      '(xelf:*updates* *game-clock* *score-1* *score-2*
	*difficult* *variation* *use-fortresses* *use-bumpers* *use-music*
	*serve-period-timer* *reset-clock*))

(setf xelf:*object-variables* 
      '(*player-1* *player-2* *goal-1* *goal-2*
	*barrier-1* *barrier-2* *ball*))

(setf xelf:*safe-variables* 
      (append *game-variables* 
	      *object-variables* 
	      xelf:*other-variables*))

(setf xelf:*terminal-bottom* (- *height* (units 1.5)))

(setf xelf:*prompt-font* xelf:*terminal-font*)

(setf xelf:*terminal-left* (units 10.4))

(defmethod find-player ((arena arena) n)
  (ecase n
    (1 (player-1))
    (2 (player-2))
    (3 (player-3))
    (4 (player-4))))

(defmethod spacebar ((arena arena)) 
  (hide-terminal))

(defclass client-arena (client-buffer arena) ())

(defmethod initialize-instance :after ((arena client-arena) &key)
  (show-prompt))

(defmethod proceed ((arena client-arena)) 
  (play-sample "go.wav"))

(defmethod populate ((arena client-arena))
  nil)

(defmethod find-input ((ship ship))
  (list 
   :time (current-time)
   :player-id (slot-value ship 'player-id)
   :stick-heading (stick-heading ship)
   :kicking-p (kicking-p ship)))

(defmethod find-local-inputs ((arena client-arena))
  (mapcar #'find-input (remove-if-not #'humanp (list *player-1* *player-2*))))

(defmethod update-input-state :after ((ship ship) plist time)
  (destructuring-bind (&key stick-heading kicking-p player-id time) plist
    (setf (input-heading ship) stick-heading)
    (setf (input-kicking-p ship) kicking-p)))

(defmethod kicking-p :around ((ship ship))
  (if (and (serverp (arena))
	   (input-p ship) 
	   (input-update-p ship))
      (input-kicking-p ship)
      (call-next-method)))

(defmethod kicking-p ((player-2 player-2))
  (if (and *netplay* (clientp (arena)))
      (or (holding-shift-p)
	  (when *player-2-joystick* 
	    (holding-button *player-2-joystick*)))
      (call-next-method)))

(defmethod stick-heading :around ((ship ship))
  (if (and 
       (serverp (arena))
       (input-p ship)
       (input-update-p ship))
      (input-heading ship)
      (call-next-method)))

(defmethod find-netplay-joystick ((arena arena)) nil)
(defmethod find-netplay-id ((arena arena)) nil)

(defclass server-arena (server-buffer arena) ())

(defmethod find-netplay-joystick ((arena server-arena)) *player-1-joystick*)
(defmethod find-netplay-joystick ((arena client-arena)) *player-2-joystick*)

(defmethod find-netplay-id ((arena server-arena)) 1)
(defmethod find-netplay-id ((arena client-arena)) 2)

(defmethod make-census ((arena arena))
  (let ((uuids (make-hash-table :test 'equal :size 64)))
    (do-nodes (node arena)
      (setf (gethash (slot-value node 'xelf::uuid) uuids)
	    (slot-value node 'xelf::uuid)))
    (setf *census* uuids)
    ;; (verbosely "Created census with ~S/~S uuids." (hash-table-count uuids)
    ;; 	       (length (get-nodes arena)))
    uuids))

(defmethod background-stream ((arena server-arena))
  (mapc #'(lambda (x) (slot-value x 'xelf::uuid))
	(nconc (find-instances arena 'wall)
	       (find-instances arena 'brick)
	       (find-instances arena 'bumper)
	       (find-instances arena 'barrier))))

(defmethod initialize-instance :after ((arena server-arena) &key)
  (setf *use-fortresses* t))

(defmethod ambient-stream ((arena server-arena))
  (let ((stream (copy-tree (make-node-stream))))
    (dolist (var *object-variables*)
      (let ((thing (symbol-value var)))
	(when (or (find thing stream :test #'object-eq)
		  (typep thing (find-class 'ball))
		  (typep thing (find-class 'ship))
		  (typep thing (find-class 'goal))
		  (typep thing (find-class 'barrier))
		  (typep thing (find-class 'bumper)))
	  (setf stream (delete (slot-value thing 'xelf::uuid)
			       stream))
	  (verbosely "Removed ~S from ambient stream." thing))))
    stream))

(defun find-arena-class (&optional netplay)
  (case netplay
    (:client 'client-arena)
    (:server 'server-arena)
    (otherwise 'arena)))

(defmethod remove-node :after ((arena client-arena) (brick brick))
  (play-sample (random-choose *color-sounds*)))

;;;;;;;;;;;;;;;;;;;

;;(defclass ball (thing)

(defclass brick (thing)
  ((hits :initform 10)
   (image :initform "brick.png")))

(defmethod initialize-instance :after ((brick brick) &key)
  (resize self (units 1) (units 1)))
  
(defmethod collide ((brick brick) (ball ball))
  (paint ball (slot-value brick 'color))
  (destroy brick))

(defmethod initialize-instance :after ((ball ball) &key)
  (setf *ball* ball)
  (resize self *ball-size* *ball-size*))

;;; Prompt widget for entering IP address at keyboard

;; (This section is not yet documented.)

(defclass ip-prompt (prompt)
  ((prompt-string :initform "Type the IP server address and then press ENTER.")))

(defmethod read-expression ((prompt ip-prompt) input-string)
  input-string)

(defmethod enter :before ((prompt ip-prompt) &optional no-clear)
  (handler-case 
      (let ((*read-eval* nil))
	(let ((result (parse-ip (slot-value prompt 'line))))
	  (if (null result)
	      (logging "Error: not a valid IP address.")
	      (progn 
		(setf *server-host* (reformat-ip result))
		(start-client (current-buffer))))))
    (condition (c)
      (logging "~S" c))))

(defun show-prompt ()
  ;;(show-terminal)
  (setf *prompt* (make-instance 'ip-prompt))
  (move-to *prompt* *terminal-left* *terminal-bottom*))

(defun hide-prompt ()
  (setf *prompt* nil))

;;; Networking functions

;; (This section is not yet documented.)

(defmethod toggle-upnp ((setup setup))
  (setf *use-upnp* (if *use-upnp* nil t)))

(defmethod start-server ((setup setup))
  (play-danceball :netplay :server 
		   :use-upnp *use-upnp*))

(defmethod start-client-prompt ((setup setup))
  (show-prompt))

(defmethod start-client ((setup setup))
  (play-danceball :netplay :client 
		   :use-upnp *use-upnp* 
		   :server-host *server-host*))

;;; Prompt widget gets events first

(defmethod handle-event :around ((setup setup) event)
  (if *prompt*
      (prog1 t (handle-event *prompt* event))
      (call-next-method)))

(defmethod draw :after ((arena arena))
  (draw-string (format nil "~S" *score-1*)
	       (units 2) 3
	       :color *player-1-color* 
	       :font *score-font*)
  (draw-string (format nil "~S" *score-2*)
	       (- *width* (units 5)) 3
	       :color *player-2-color* 
	       :font *score-font*)
  (draw-string (game-clock-string) 
	       (units 31.6) 3
	       :color "white"
	       :font *score-font*)
  (draw-string "[Arrows/NumPad] move     [Shift] kick      [Escape] game setup     [PageDown] reset game     [PageUp] select variation     [Control-Q] quit      [Control-M] music on/off"
	       (units 2.6) (- *height* 17)
	       :color "white"
	       :font *score-font*)
  (unless (game-on-p)
    (draw-string "END OF REGULATION"
		 (units 36) (units 2)
		 :color "white"
		 :font *big-font*))
  (when (and (not (both-joysticks-connected))
	     (not *netplay*))
    (draw-string (if (not (difficult-p)) "NORMAL AI" "ADVANCED AI")
		 (units 48) 3
		 :color *player-2-color*
		 :font *score-font*))
  (when *netplay*
    (draw-string (ecase *netplay*
		   (:client "CLIENT: PLAYER 2")
		   (:server "SERVER: PLAYER 1"))
		 (units 48) 3
		 :color (ecase *netplay*
			  (:client *player-2-color*)
			  (:server *player-1-color*))
		 :font *score-font*))
  ;; draw gray bars under goal slot to prevent color problems
  (when (connectedp arena)
    (let ((x1 (slot-value (goal-1) 'x))
	  (x2 (slot-value (goal-2) 'x))
	  (y (units 1))
	  (width (units 1.3))
	  (height (units 34)))
      ;;(draw-box (- x1 3) y width height :color "gray30")
      (draw (goal-1))
      ;;(draw-box (- x2 3) y width height :color "gray30")
      (draw (goal-2))
      (draw (ball))
      (draw (player-1))
      (draw (player-2)))))


;;; User Interface

;; We need to draw the score, game clock, and help strings on top of the
;; arena.

;; First we choose from the preset named font styles declared in
;; [[https://gitlab.com/dto/xelf/blob/4.3/standard/index.xelf][index.xelf]]. (That file shows how to make custom styles, as (for
;; example) for the remainder of the included Bitstream Vera fonts.

(defparameter *score-font* "sans-mono-bold-12")
(defparameter *big-font* "sans-mono-bold-16")

;; The default [[file:dictionary/DRAW.html][DRAW]] method for a buffer renders the background and game
;; objects. After this, we'd like to overlay our user interface.

;; See also [[file:dictionary/DRAW-STRING.html][DRAW-STRING]].


;;; A special buffer for displaying text

(defparameter *button-time* 30)
(defparameter *ready-time* 120)

(defclass setup (buffer)
  ((timer :initform 0)
   (player :initform 1)
   (background-color :initform "CornflowerBlue")))

(defmethod update :after ((setup setup))
  (with-slots (timer player) setup
    (when (plusp timer) 
      (decf timer))
    (when (and (zerop timer)
	       (null player))
      (stop setup)
      (do-reset))))

;;; Arena command for opening the setup screen

(defmethod setup ((arena arena))
  (stop arena)
  (at-next-update 
    (switch-to-buffer (make-instance 'setup))
    (destroy arena)))

;;; Binding key commands at initialization

(defmethod initialize-instance :after ((setup setup) &key)
  (bind-event setup '(:s :control) 'start-server)
  (bind-event setup '(:c :control) 'start-client-prompt)
  (bind-event setup '(:u :control) 'toggle-upnp)
  (hide-terminal)
  (resize setup *width* *height*))

;;; Prompt message strings

;; These strings are segregated here for easier localization and editing.

(defparameter *p1-prompt* "Press any button on Gamepad 1, or Spacebar to use the keyboard.")
(defparameter *p2-prompt-1* "To play against a friend, press any button on Gamepad 2.")
(defparameter *p2-prompt-2* "Or, press any button on Gamepad 1 to play against the computer.")

(defun ready-prompt ()
  (if *netplay*
      "Local player versus network. Get ready!"
      (if (null *player-2-joystick*)
	  "Player 1 versus the computer. Get ready!"
	  "Player 1 versus Player 2. Get Ready!")))

(defparameter *must* "(Gamepads must be plugged in before the application is started.)")
(defparameter *net* "Online Play: Press Control-S to start server,  Control-C for client. Press Control-U to toggle UPnP before starting.")

;;; Highlighted prompt line is rendered with a flicker effect

(defun flicker () (random-choose '("white" "cyan")))

;;; Drawing the setup screen

(defmethod draw :after ((setup setup))
  (when (null *prompt*)
    (with-slots (timer player) setup
      (draw-string "Game setup" (units 28.3) (units 2) :color "white" :font "sans-mono-bold-22")
      (draw-string *must* (units 20) (units 5) :color "white" :font "sans-mono-bold-12")
      (draw-string (if *use-upnp* "UPnP Enabled" "UPnP Disabled") (units 20) (units 9) :color "white" :font "sans-mono-bold-12")
      (draw-string 
       (if *netplay* 
	   (format nil "Online play enabled in ~S mode." *netplay*)
	   *net*)
       (units 10) (units 7) :color "white" :font "sans-mono-bold-12")
      (case player 
	(1 (draw-string *p1-prompt* (units 12) (units 12) :color (flicker) :font *big-font*))
	(2 (draw-string *p2-prompt-1* (units 12) (units 14) :color (flicker) :font *big-font*)
	   (draw-string *p2-prompt-2* (units 12) (units 16) :color "white" :font *big-font*)))
      (when (null player)
	(draw-string (ready-prompt) (units 12) (units 18) :color "white" :font *big-font*))))
  (when *prompt*
    (draw *prompt*)))

;;; Handling key and button presses

(defmethod handle-event :after ((setup setup) event)
  (with-slots (timer player) setup
    (when (and (consp (first event))
	       (eq :space (first (first event))))
      (setf timer *ready-time*)
      (setf player nil))
    (when (and (eq :joystick (first event))
	       (not (plusp timer)))
      (destructuring-bind (which button direction) (rest event)
	(case player
	  (1 
	   (if (not (clientp (arena)))
	       (progn (setf *player-1-joystick* which)
		      (setf *player-2-joystick* nil)
		      (setf timer *button-time*)
		      (setf player 2))
	       (progn (setf *player-1-joystick* nil)
		      (setf *player-2-joystick* which)
		      (setf timer *ready-time*)
		      (setf player nil))))
	  (2 
	   (if (= which *player-1-joystick*)
	       ;; player chose vs AI 
	       (progn 
		 (setf *player-2-joystick* nil)
		 (setf timer *ready-time*)
		 (setf player nil))
	       (progn 
		 (setf *player-2-joystick* which)
		 (setf timer *ready-time*)
		 (setf player nil)))))))))
