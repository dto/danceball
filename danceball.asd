(asdf:defsystem #:danceball
  :depends-on (:xelf)
  :components ((:file "package")
               (:file "danceball" :depends-on ("package"))
               (:file "arena" :depends-on ("danceball"))
               (:file "things" :depends-on ("arena"))
               (:file "enemy" :depends-on ("things"))
               (:file "player" :depends-on ("enemy"))
               (:file "ball" :depends-on ("player"))
               (:file "menu" :depends-on ("ball"))
               (:file "draw" :depends-on ("menu"))
               (:file "dj" :depends-on ("draw"))))

