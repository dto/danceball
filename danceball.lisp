(in-package :danceball)

(defparameter *title-string* "Danceball v0.8")

(defparameter *danceball-copyright-notice*
"Welcome to Danceball. 
Danceball and Xelf are Copyright (C) 2006-2017, 2021 by David T. O'Toole 
email: deeteeoh1138@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

Full license text of the GNU Lesser General Public License is in the
enclosed file named 'COPYING'. Full license texts for compilers,
assets, libraries, and other items are contained in the LICENSES
directory included with this application.

On some platforms, binaries of libSDL 1.2 are redistributed with
XELF. libSDL 1.2 is under the LGPL license; see the \"licenses\"
subdirectory for full text. Some functions in the file logic.lisp are
based on code written by Peter Norvig in his book 'Paradigms of
Artificial Intelligence Programming'. See logic.lisp for details.
Some of the OpenGL functions in console.lisp are derived from code in
Bart Botta's CL-OPENGL tutorials; see
http://3bb.cc/tutorials/cl-opengl/ This program includes the free
DejaVu fonts family in the subdirectory ./standard/. For more
information, see the file named DEJAVU-FONTS-LICENSE.txt in the
xelf/licenses subdirectory.  Please see the included text files
\"COPYING\" and \"CREDITS\" for more information.
")

(defun show-copyright-notice ()
  (mapcar #'message (split-string-on-lines
                     (concatenate 'string 
                                  *danceball-copyright-notice*
                                  xelf::*compiler-copyright-notice*))))

;;; Configuration files

(defun read-user-config ()
  (let ((file (xelf::find-project-file "danceball" "config.lisp")))
    (when (cl-fad:file-exists-p file)
      (eval (read-from-string
             (xelf::read-file* file))))))

(defun read-songs ()
  (let ((file (xelf::find-project-file "danceball" "songs.lisp")))
    (when (cl-fad:file-exists-p file)
      (load file))))

;;; Screen overlay images

(defresource "cleared.png")
(defresource "failed.png")

;;; GC

(defun collect-garbage ()
  #+ccl (ccl:gc)
  #+sbcl (sb-ext:gc))

;;; String utility functions

(defun-memo pretty-string (thing)
    (:key #'first :test 'equal :validator #'identity)
  (let ((name (etypecase thing
		(symbol (symbol-name thing))
		(string thing))))
    (coerce 
     (substitute #\Space #\- 
		 (string-downcase 
		  (string-trim " " name)))
     'simple-string)))

(defun-memo ugly-symbol (string)
    (:key #'first :test 'equal :validator #'identity)
  (intern 
   (string-upcase
    (substitute #\- #\Space 
		(string-trim " " string)))))

;;; Displaying messages to the player

(defparameter *message-timeout* (seconds->frames 3.0))

(defvar *message-timer* 0)

(defvar *message-string* nil)

(defun initialize-message-timer (&optional (timeout *message-timeout*))
  (setf *message-timer* timeout))

(defun update-message-timer ()
  (when (plusp *message-timer*)
    (decf *message-timer*)))

(defun message-should-display-p ()
  (plusp *message-timer*))

(defun display-message (string &optional (timeout *message-timeout*))
  (setf *message-string* string)
  (initialize-message-timer timeout))

;;; Difficulty parameterization

(defparameter *difficulty* 1)

(defun with-difficulty (&rest args)
  (if (<= (length args) *difficulty*)
      (nth (1- (length args)) args)
      (nth *difficulty* args)))

;; works out to reduce X above 110bpm and decrease it below.
;; this assumes the slope is positive.
(defun adjust-slope-for-bpm (x bpm)
  (* x (/ 110 bpm))) 

(defun with-difficulty* (&rest args)
  (adjust-slope-for-bpm (apply #'with-difficulty args)
                        *beats-per-minute*))

;;; Display properties

(setf xelf:*unit* 42) ;; establish grid size
(defparameter *width* 1280) ;; default window size
(defparameter *height* 720)
(defparameter *cell-width* (truncate (units 1)))
(defparameter *cell-height* (truncate (units 1)))

(defun initialize-screen ()
  (setf *frame-rate* 60)
  (setf *font-texture-scale* 2)
  (setf *font-texture-filter* :nearest)
  (setf *window-title* *title-string*)
  (setf *screen-width* *width*)
  (setf *screen-height* *height*)
  (setf *nominal-screen-width* *width*)
  (setf *nominal-screen-height* *height*)
  (setf *scale-output-to-window* t)
  (setf *use-antialiased-text* t))

;; TrueType Font

(defresource (:name "sideways" :type :ttf :file "sideways.ttf" :properties (:size 24)))
(defresource (:name "sideways-large" :type :ttf :file "sideways.ttf" :properties (:size 40)))
(defresource (:name "sideways-huge" :type :ttf :file "sideways.ttf" :properties (:size 80)))
(defresource (:name "sideways-mega" :type :ttf :file "sideways.ttf" :properties (:size 120)))
(setf xelf:*font* "sideways")

;;; Resource definition utilities

(defun image-set (name count &optional (start 1))
  (loop for n from start to count
	collect (format nil "~A-~S.png" name n)))

(defun sample-set (name count &optional (start 1))
  (loop for n from start to count
	collect (format nil "~A-~S.wav" name n)))

;;; Time and tempo calculations

(defvar *beats-per-minute* 120)
(defvar *beats-per-measure* 4)
(defparameter *default-timing-window* 120)
(defvar *last-beat* 0)
(defvar *start-time* 0)
(defvar *beat-hook* ())

(defparameter *marvelous* 16.7)
(defparameter *perfect* 33)
(defparameter *great* 92)
(defparameter *good* 142)
(defparameter *boo* 225)

(defun interval-rating (t0 t1)
  (let ((interval (abs (- t0 t1))))
    (cond ((<= interval *marvelous*) :marvelous)
          ((<= interval *perfect*) :perfect)
          ((<= interval *great*) :great)
          ((<= interval *good*) :good)
          ((<= interval *boo*) :boo)
          (t :miss))))

(defun interval-miss-p (t0 t1)
  (eq :miss (interval-rating t0 t1)))

(defun interval-hit-p (t0 t1)
  (not (interval-miss-p t0 t1)))

(defun seconds-per-beat (&optional (beats-per-minute *beats-per-minute*))
  (/ 1.0
     (/ beats-per-minute 60.0)))

(defun seconds->frames (seconds) (truncate (* 60 seconds)))

(defun milliseconds-per-beat (&optional (beats-per-minute *beats-per-minute*))
  (* 1000.0 (seconds-per-beat beats-per-minute)))

(defun milliseconds-per-dance-frame (&optional (beats-per-minute *beats-per-minute*))
  ;; there are 8 frames per dance beat in the spritesheet
  (/ (milliseconds-per-beat beats-per-minute) 8.0))

(defun simultaneous-p (t0 t1)
  (>= *default-timing-window* (abs (- t0 t1))))

(defun get-internal-real-time* ()
  #+sbcl (truncate (/ (get-internal-real-time) 1000))
  #+ccl (truncate (/ (get-internal-real-time) 1000)))

(defun elapsed-time* ()
  (- (get-internal-real-time*) *start-time*))

(defvar *elapsed-time-this-update* 0)

(defun elapsed-time ()
  *elapsed-time-this-update*)

(defun elapsed-beats ()
  (truncate (/ (elapsed-time)
               (milliseconds-per-beat))))

(defun beat-remainder ()
  (- (elapsed-time)
     (* (elapsed-beats)
        (milliseconds-per-beat))))

(defun current-dance-frame-index ()
  (truncate (* 8.0 (/ (float (beat-remainder))
                      (float (milliseconds-per-beat))))))

(defun on-beat-p ()
  (>= *default-timing-window*
      (rem (elapsed-time)
           (milliseconds-per-beat))))

(defun on-half-beat-p ()
  (>= *default-timing-window*
      (rem (elapsed-time)
           (/ (milliseconds-per-beat) 2.0))))

(defun only-on-half-beat-p ()
  (and (not (on-beat-p))
       (on-half-beat-p)))

(defun initialize-beat (&optional (beats-per-minute *beats-per-minute*))
  (setf *beats-per-minute* beats-per-minute)
  (setf *start-time* (get-internal-real-time*))
  (setf *elapsed-time-this-update* 0)
  (setf *last-beat* 0))

(defun update-elapsed-time ()
  (setf *elapsed-time-this-update* (elapsed-time*)))

(defun update-beat ()
  (when (< *last-beat* (elapsed-beats))
    (incf *last-beat*)
    (next-beat)
    (when (arena)
      (handle-beat (arena)))))

(defvar *beat-properties* ())

(defun load-beat-properties (file)
  (setf *beat-properties* (xelf::load-scheme-file file)))

(defun current-beat-properties ()
  (when *beat-properties*
    (car *beat-properties*)))

(defun beat-property (props name)
  (cdr (assoc name props)))

(defun current-beat-property (name)
  (beat-property (current-beat-properties) name))

(defun next-beat ()
  (pop *beat-properties*))

(defun kick-playing-p ()
  (member 'cl-user::kick (current-beat-property 'cl-user::percussion-3-guess-multiple)))

(defun snare-playing-p ()
  (member 'cl-user::snare (current-beat-property 'cl-user::percussion-3-guess-multiple)))

(defun hat-playing-p ()
  (member 'cl-user::cymbal (current-beat-property 'cl-user::percussion-3-guess-multiple)))

;;; Music resources

(defvar *current-song* nil)
(defvar *current-song-length* 0)

(defresource "freakstyle.ogg" :volume 90 :bpm 64 :title "Freakstyle" :artist "DTO")
(defresource "hauntedhouse.ogg" :volume 90 :bpm 130 :title "Haunted House" :artist "DTO")
(defresource "electro.ogg" :volume 90 :bpm 125 :title "Electro" :artist "DTO")
(defresource "2084.ogg" :volume 90 :bpm 132 :title "2084" :artist "DTO")
(defresource "nightclubbing.ogg" :volume 90 :bpm 138 :title "Nightclubbing" :artist "DTO")

(defparameter *demo-songs*
  '("freakstyle" "hauntedhouse" "electro" "2084" "nightclubbing"))

(defparameter *level-songs* *demo-songs*)

(defun choose-song ()
  (random-choose *level-songs*))

(defun song-ogg (name)
  (if (search ".ogg" name)
      name
      (concatenate 'string name ".ogg")))

(defun song-bpm (name)
  (find-resource-property (song-ogg name) :bpm))

(defun song-artist (name)
  (find-resource-property (song-ogg name) :artist))

(defun song-title (name)
  (find-resource-property (song-ogg name) :title))

;;; Sound FX resources

(setf xelf:*output-chunksize* 512) ;; set latency as low as is practical

(defresource "applause.wav" :volume 80)
(defresource "spawn.wav" :volume 60)
(defresource "go.wav" :volume 23)
(defresource "woop.wav" :volume 60)
(defresource "clear.wav" :volume 60)
(defresource "boost.wav" :volume 60)
(defresource "explosion-1.wav" :volume 40)
(defresource "serve.wav" :volume 90)
(defresource "grab.wav" :volume 23)
(defresource "bounce.wav" :volume 20)
(defresource "newball.wav" :volume 70)
(defresource "return.wav" :volume 30)
(defresource "error.wav" :volume 40)

(defresource "keyclick.wav" :volume 40)
(defresource "select.wav" :volume 40)
(defresource "menu.wav" :volume 40)

(defparameter *damage-sounds* (sample-set "damage" 3))
(defparameter *wubble-sounds* (sample-set "wubble" 3))

(defresource 
    (:name "wubble-1.wav" :type :sample :file "wubble-1.wav" :properties (:volume 70))
    (:name "wubble-2.wav" :type :sample :file "wubble-2.wav" :properties (:volume 70))
  (:name "wubble-3.wav" :type :sample :file "wubble-3.wav" :properties (:volume 70)))

(defresource "megahit.wav" :volume 128)

(defresource 
    (:name "damage-1.wav" :type :sample :file "damage-1.wav" :properties (:volume 30))
    (:name "damage-2.wav" :type :sample :file "damage-2.wav" :properties (:volume 30))
  (:name "damage-3.wav" :type :sample :file "damage-3.wav" :properties (:volume 30)))

(defresource 
    (:name "boop1.wav" :type :sample :file "boop1.wav" :properties (:volume 20))
    (:name "boop2.wav" :type :sample :file "boop2.wav" :properties (:volume 20))
  (:name "boop3.wav" :type :sample :file "boop3.wav" :properties (:volume 20)))

(defparameter *bounce-sounds* '("boop1.wav" "boop2.wav" "boop3.wav"))

(defresource 
    (:name "doorbell1.wav" :type :sample :file "doorbell1.wav" :properties (:volume 23))
    (:name "doorbell2.wav" :type :sample :file "doorbell2.wav" :properties (:volume 23))
  (:name "doorbell3.wav" :type :sample :file "doorbell3.wav" :properties (:volume 23)))

(defparameter *doorbell-sounds* '("doorbell1.wav" "doorbell2.wav" "doorbell3.wav"))

(defparameter *slam-sounds*
  (defresource 
      (:name "slam1.wav" :type :sample :file "slam1.wav" :properties (:volume 52))
      (:name "slam2.wav" :type :sample :file "slam2.wav" :properties (:volume 52))
    (:name "slam3.wav" :type :sample :file "slam3.wav" :properties (:volume 52))))

(defresource 
    (:name "whack1.wav" :type :sample :file "whack1.wav" :properties (:volume 42))
    (:name "whack2.wav" :type :sample :file "whack2.wav" :properties (:volume 42))
  (:name "whack3.wav" :type :sample :file "whack3.wav" :properties (:volume 42)))

(defparameter *whack-sounds* '("whack1.wav" "whack2.wav" "whack3.wav"))

(defresource 
    (:name "color1.wav" :type :sample :file "color1.wav" :properties (:volume 32))
    (:name "color2.wav" :type :sample :file "color2.wav" :properties (:volume 32))
  (:name "color3.wav" :type :sample :file "color3.wav" :properties (:volume 32)))

(defparameter *color-sounds* '("color1.wav" "color2.wav" "color3.wav"))

(defresource "block.wav" :volume 80)
(defparameter *bip-sounds* (sample-set "bip" 4))
(defresource "bip-1.wav" :volume 30)
(defresource "bip-2.wav" :volume 30)
(defresource "bip-3.wav" :volume 30)
(defresource "bip-4.wav" :volume 30)

;;; Game controller setup

(defparameter *buttons* '(:up :down :left :right :cross :circle :left-bumper :right-bumper))

(defparameter *axis-0-negative* -32768)
(defparameter *axis-0-positive* 32768)

(defparameter *controller-profiles* nil)
  ;; `(("   ESM-9013"
  ;;    :button-map (:cross 2 :circle 1 :left-bumper 4 :right-bumper 5)
  ;;    :axis-map (:up -1 :down +1 :left ,*axis-0-negative* :right ,*axis-0-positive*))
  ;;   ("My-Power CO.,LTD. PS(R) Controller Adaptor"
  ;;    :button-map (:up 13 :down 14 :left 15 :right 16 :cross 0 :circle 1 :left-bumper 4 :right-bumper 5))
  ;;   ("USB Joystick"
  ;;     :button-map (:up 0 :down 1 :left 2 :right 3 :cross 6 :circle 7))))

(defparameter *player-1-joystick* 0)
(defparameter *player-2-joystick* nil)

(defclass keyboard-controller () ())

(defmethod button-pressed-p ((controller keyboard-controller) button)
  (ecase button
    (:up (or (holding-up-arrow-p)
             (keyboard-down-p :w)))
    (:down (or (holding-down-arrow-p)
               (keyboard-down-p :s)))
    (:left (or (holding-left-arrow-p)
               (keyboard-down-p :a)))
    (:right (or (holding-right-arrow-p)
                (keyboard-down-p :d)))
    ((:cross :circle) (or (holding-shift-p)
                          (keyboard-down-p :return)
                          (keyboard-down-p :space)))
    ((:left-bumper (xelf::holding-control)))
    ((:right-bumper (xelf::holding-alt)))))

(defclass controller (keyboard-controller)
  ((device-id :initform 0 :initarg :device-id :accessor device-id)
   (button-map :initform '(:cross 2 :circle 1 :left-bumper 4 :right-bumper 5)
               :initarg :button-map :accessor button-map)
   (axis-map :initform `(:up -1 :down +1 :left ,*axis-0-negative* :right ,*axis-0-positive*)
             :initarg :axis-map :accessor axis-map)))

(defmethod find-map ((controller controller) button)
  (let ((value (getf (button-map controller) button)))
    (if value
        (list 'cl-user::button value)
        (let ((value2 (getf (axis-map controller) button)))
          (when value2
            (list 'cl-user::axis value2))))))

(defmethod button-pressed-p ((controller controller) button)
  (if (getf (button-map controller) button)
      ;; it's a button, read the state
      (joystick-button-pressed-p (getf (button-map controller) button)
                                 (device-id controller))
      (when (getf (axis-map controller) button)
        ;; it's an axis. read the state
        (let* ((axis* (getf (axis-map controller) button))
               (axis-pre (abs axis*))
               (axis (if (= *axis-0-positive* axis-pre)
                         0
                         axis-pre))
               (positive (plusp axis*))
               (raw-value (joystick-axis-raw-value axis (device-id controller))))
          (if positive
              (>= raw-value xelf::*joystick-dead-zone*)
              (<= raw-value (- 0 xelf::*joystick-dead-zone*)))))))

(defvar *controller-1* nil)
(defvar *keyboard-controller* nil)

(defparameter *default-controller-profile*
  `(:button-map
    (:cross 2 :circle 1)
    :axis-map
    (:up -1 :down +1 :left ,*axis-0-negative* :right ,*axis-0-positive*)))

(defun choose-controller-profile ()
  (let* ((name (sdl:sdl-joystick-name *player-1-joystick*))
         (profile (assoc name *controller-profiles* :test 'equal)))
    (if profile
        (progn
          (message "Found profile for controller ~A..." name)
          (setf *controller-1* (apply #'make-instance 'controller
                                      :device-id *player-1-joystick*
                                      (cdr profile))))
        (progn
          (message "Could not find profile for controller ~A. Using default profile..." name)
          (setf *controller-1* (apply #'make-instance 'controller
                                      :device-id *player-1-joystick*
                                      *default-controller-profile*))))))

(defparameter *initial-button-press-states*
  '(:up nil :down nil :left nil :right nil :cross nil :circle nil :left-bumper nil :right-bumper nil))

(defparameter *initial-button-press-times*
  '(:up 0 :down 0 :left 0 :right 0 :cross 0 :circle 0 :left-bumper 0 :right-bumper 0))

(defvar *button-press-states* nil)

(defun button-press-state (button)
  (getf *button-press-states* button))

(defun set-button-press-state (button state)
  (setf (getf *button-press-states* button) state))

(defsetf button-press-state set-button-press-state)

(defvar *button-press-times* nil)

(defun button-press-time (button)
  (getf *button-press-times* button))

(defun set-button-press-time (button time)
  (setf (getf *button-press-times* button) time))

(defsetf button-press-time set-button-press-time)

(defun initialize-button-states ()
  (setf *button-press-states* (copy-tree *initial-button-press-states*))
  (setf *button-press-times* (copy-tree *initial-button-press-times*)))

(defparameter *press-timeout* 50)

(defun button-recently-pressed-p (button)
  (>= *press-timeout* (- (elapsed-time)
                         (button-press-time button))))

(defun press-button (button)
  (unless (button-recently-pressed-p button)
    (setf (button-press-state button) t)
    (setf (button-press-time button) (elapsed-time))))

(defun release-button (button)
  (setf (button-press-state button) nil))

(defun button-actually-pressed-p (button)
  (when (or (menu-p (current-buffer))
            (not (and (dj)
                      (input-suspended-p (dj)))))
    (let ((value (or (button-pressed-p *keyboard-controller* button)
                     (button-pressed-p *controller-1* button))))
      (when value
        (setf (button-press-time button) 0))
      value)))

(defun update-button-states ()
  (dolist (button *buttons*)
    (if (not (button-actually-pressed-p button))
        (release-button button)
        ;; don't press if still holding from last update
        (unless (button-press-state button)
          (press-button button)))))

(defun button-held-p (button)
  (and (button-actually-pressed-p button)
       (not (button-recently-pressed-p button))))

;;; Spritesheets and animation

(defparameter *slide-frames* 8)
(defparameter *default-frame-delay* 8)

(defparameter *spritesheet* "spritesheet.png")
(defresource "spritesheet.png"
  :tile-height 32
  :tile-width 32)

(defparameter *spritesheet-glow* "spritesheet-glow.png")
(defresource "spritesheet-glow.png"
  :tile-height 32
  :tile-width 32)

(defparameter *animations* 
  '(:player-dance-up (:row 8 :start 1 :end 8 :offset 8)
    :player-dance-down (:row 9 :start 1 :end 8 :offset 8)
    :player-dance-left (:row 10 :start 1 :end 8 :offset 8)
    :player-dance-right (:row 11 :start 1 :end 8 :offset 8)
    :player-color-up (:row 8 :start 9 :end 16 :offset 16)
    :player-color-down (:row 9 :start 9 :end 16 :offset 16)
    :player-color-left (:row 10 :start 9 :end 16 :offset 16)
    :player-color-right (:row 11 :start 9 :end 16 :offset 16)
    :player-damage  (:row 1 :start 9 :end 12)
    :player-death (:row 1 :start 13 :end 16)
    ;;
    :bullet-dance-up (:row 14 :start 9 :end 16)
    :bullet-dance-down (:row 15 :start 9 :end 16)
    :bullet-dance-left (:row 16 :start 9 :end 16)
    :bullet-dance-right (:row 17 :start 9 :end 16)
    ;;
    :robot-1-dance (:row 2 :start 1 :end 8 :offset 4)
    :robot-1-damage  (:row 2 :start 9 :end 12)
    :robot-1-death (:row 2 :start 13 :end 16)
    ;; 
    :robot-2-dance (:row 3 :start 1 :end 8 :offset 4)
    :robot-2-damage  (:row 3 :start 9 :end 12)
    :robot-2-death (:row 3 :start 13 :end 16)
    ;; 
    :robot-3-dance (:row 4 :start 1 :end 8 :offset 6)
    :robot-3-damage  (:row 4 :start 9 :end 12)
    :robot-3-death (:row 4 :start 13 :end 16)
    ;; 
    :biclops-dance (:row 5 :start 1 :end 8 :offset 6)
    :biclops-damage  (:row 5 :start 9 :end 12)
    :biclops-death (:row 5 :start 13 :end 16)
    ;;
    :humanoid-dance (:row 6 :start 1 :end 8 :offset 4)
    :humanoid-damage  (:row 6 :start 9 :end 12)
    :humanoid-death (:row 6 :start 13 :end 16)
    ;;
    :star-dance (:row 12 :start 1 :end 8 :offset 4)
    :waveform-dance (:row 12 :start 9 :end 16)
    :box-dance (:row 13 :start 9 :end 16)
    :grid-dance (:row 7 :start 1 :end 8)
    :brick-dance (:row 7 :start 9 :end 16)
    :mine-dance (:row 18 :start 9 :end 16)
    :bomb-dance (:row 19 :start 9 :end 16)
    ;;
    :up (:row 14 :start 1 :end 4 :offset 4)
    :down (:row 15 :start 1 :end 4 :offset 4)
    :left (:row 16 :start 1 :end 4 :offset 4)
    :right (:row 17 :start 1 :end 4 :offset 4)
    :cross (:row 18 :start 1 :end 4 :offset 4)
    :circle (:row 19 :start 1 :end 4 :offset 4)))

(defun single-frame (spritesheet row column)
  (list spritesheet row column))

(defun ball-outer-frame ()
  (single-frame *spritesheet* 13 1))

(defun ball-inner-frame ()
  (single-frame *spritesheet* 13 2))

(defun shield-horizontal-frame ()
  (single-frame *spritesheet* 13 4))

(defun shield-vertical-frame ()
  (single-frame *spritesheet* 13 5))

(defun arrow-background-frame (direction)
  (append (list *spritesheet*
                (ecase direction
                  (:up (list 14 5))
                  (:down (list 15 5))
                  (:left (list 16 5))
                  (:right (list 17 5))
                  (:cross (list 18 5))
                  (:circle (list 19 5))))))

(defun spritesheet-tile-width (spritesheet)
  (find-resource-property spritesheet :tile-width))

(defun spritesheet-tile-height (spritesheet)
  (find-resource-property spritesheet :tile-height))

(defun spritesheet-width (spritesheet)
  (image-width spritesheet))

(defun spritesheet-height (spritesheet)
  (image-height spritesheet))

(defun spritesheet-animation-frames-* (spritesheet parameters)
  (destructuring-bind (&key row start end mirror offset
			    (delay *default-frame-delay*)
		       &allow-other-keys) parameters
    (let ((frames (loop for column from start
	                collect (list spritesheet row column mirror delay)
	                while (< column end))))
      (if (null offset)
          frames
          (append
           ;; flip halves to start beat animation at OFFSET frame if needed
           (subseq frames (- offset start))
           (subseq frames 0 (- offset start)))))))

;; caching frontend
(defun-memo spritesheet-animation-frames (spritesheet parameters)
    (:key #'identity :test #'equal)
  (spritesheet-animation-frames-* spritesheet parameters))

(defun frame-bounding-box-* (frame)
  (destructuring-bind (spritesheet row column &optional mirror delay) frame
    (let* ((tile-height (spritesheet-tile-height spritesheet))
	   (tile-width (spritesheet-tile-width spritesheet))
	   (file-height (spritesheet-height spritesheet))
	   (file-width (spritesheet-width spritesheet))
	   (top (* row tile-height))
	   (left (* column tile-width))
	   (right (+ left tile-width))
	   (bottom (+ top tile-height)))
      (when (eq mirror :horizontal)
	(rotatef left right))
      (list
       (/ (+ top 0.5) file-height)
       (/ (+ left 0.5) file-width)
       (/ (- right 0.5) file-width)
       (/ (- bottom 0.5) file-height)
       ))))

;; caching frontend
(defun-memo frame-bounding-box (frame)
    (:key #'identity :test #'equal)
  (frame-bounding-box-* frame))

(defun frame-delay (frame)
  (fifth frame))

(defun find-glow-frame (frame)
  (cons *spritesheet-glow* (rest frame)))

(defun draw-frame
    (frame x y &optional (width (units 1))
			 (height (units 1))
                         angle color)
  (destructuring-bind (v1 u1 u2 v2)
      (frame-bounding-box frame)
    (let ((left x)
	  (right (+ x width))
	  (top y)
	  (bottom (+ y height))
	  (spritesheet (first frame)))
      (xelf::enable-texture-blending)	
      (set-blending-mode :alpha)
      (gl:bind-texture :texture-2d (find-texture spritesheet))
      (gl:matrix-mode :modelview)
      (xelf::set-vertex-color (or color "white"))
      (gl:with-pushed-matrix 
	(gl:load-identity)
        (gl:with-primitive :quads
	  (gl:tex-coord u1 v2)
	  (gl:vertex x bottom 0)
	  (gl:tex-coord u2 v2)
	  (gl:vertex right bottom 0)
	  (gl:tex-coord u2 v1)
	  (gl:vertex right y 0)
	  (gl:tex-coord u1 v1)
	  (gl:vertex x y 0))))))

;;; Grid definitions

(defparameter *grid-directions* '(:right :up :left :down))
(defparameter *grid-diagonals* '(:upright :upleft :downright :downleft))

(defmethod grid-offset ((buffer buffer)) 0)
(defmethod grid-object-size ((buffer buffer)) (units 1))

(defun grid-position (x y)
  (values (units x) (units y)))

(defun grid-position-center (x y)
  (values (cfloat (+ (units x) (units 0.5)))
	  (cfloat (+ (units y) (units 0.5)))))

;; (defun valid-position-p (x y)
;;   (and (not (minusp x))
;;        (not (minusp y))
;;        (< x (width-in-units))
;;        (< y (height-in-units))))

(defun grid-position-bounding-box (x y)
  ;; top left right bottom
  (values (units y) 
	  (units x) 
	  (units (1+ x)) 
	  (units (1+ y))))

(defparameter *margin-width* 0.5)

(defun grid-object-bounding-box (x y)
  (values (+ (units y) *margin-width*)
	  (+ (units x) *margin-width*)
	  (- (units (1+ x)) *margin-width*)
	  (- (units (1+ y)) *margin-width*)))

;;; Color definitions

(defparameter *gradient-images* (image-set "gradient" 8))

(defparameter *player-1-color* "hot pink")
(defparameter *player-2-color* "orange")

(defparameter *player-1-fortress-colors* '("dark orchid" "medium orchid" "orchid"))
(defparameter *player-2-fortress-colors* '("dark orange" "orange" "gold"))
(defparameter *traditional-robot-colors* '("gold" "olive drab" "RoyalBlue3" "dark orchid"))

(defparameter *wall-color* "gray20")

(defparameter *depth* 0)

(defun with-depth (&rest args)
  (if (<= (length args) *depth*)
      (nth (1- (length args)) args)
      (nth *depth* args)))

(defparameter *two-brick-themes* 
  '((:snefru "DarkSlateBlue" "green" 
     "magenta" "cyan")
    (:xalcrys "black" "blue violet" 
     "deep pink" "cornflower blue")
    (:zupro "olive drab" "hot pink" 
     "cyan" "yellow")))

(defparameter *three-brick-themes*
  '((:snafu "dark magenta" "gray20" 
     "cyan" "red" "yellow")
    (:atlantis "maroon" "dodger blue" 
     "pale green" "hot pink" "red")
    (:radium "dark olive green" "gold"
     "cyan" "chartreuse" "magenta")
    (:krez "dark orchid" "maroon2" 
     "green" "red" "yellow")
    (:zerk "black" "gray40"
     "yellow green" "orange" "cornflower blue")
    (:tandy "DarkSlateBlue" "gray80" 
     "yellow" "cyan" "deep pink")
    (:zendium "gray17" "orchid"
     "deep pink" "deep sky blue" "chartreuse")
    (:command "dim gray" "yellow" 
     "cyan" "deep pink" "green yellow")))

(defvar *red-green-color-blindness* nil)

(defparameter *red-green-color-blindness-theme* 
  '("cornflower blue" "yellow" "pale green" "violet red"))

(defun red-green-color-blindness-theme (&optional (colors 4))
  (append (list "black" "gray50")
	  (subseq (derange *red-green-color-blindness-theme*) 
		  0 colors)))

(defparameter *themes* 
  (append *two-brick-themes* *three-brick-themes*))

(defun find-theme (name)
  (rest (assoc name *themes*)))

(defparameter *theme* '("black" "white" "red" "blue"))

(defun theme-colors (&optional (theme *theme*))
  (rest (rest theme)))

(defun color-hash (color)
  (let ((pos (position color (theme-colors) :test 'equal)))
    (when pos (1+ pos))))

(defun set-theme (&optional (theme :wizard))
  (setf *theme* 
	(if (consp theme) 
	    theme
	    (find-theme theme))))

(defun random-theme () (random-choose (mapcar #'car *themes*)))

(defun set-random-theme () (set-theme (random-theme)))

(defun background-color ()
  (when *theme* (first *theme*)))

(defun wall-color ()
  (when *theme* (second *theme*)))

(defun level-colors ()
  (when *theme* (rest (rest *theme*))))

;;; Global objects

(defvar *ball* nil)
(defun ball () *ball*)
(defun set-ball (ball) (setf *ball* ball))

(defvar *arena* nil)
(defun arena () *arena*)
(defvar *player-1* nil)
(defvar *player-2* nil)
(defun player-1 () *player-1*)
(defun player-2 () *player-2*)
(defun set-player-1 (x) (setf *player-1* x))
(defun set-player-2 (x) (setf *player-2* x))

(defun player-1-p (x) (eq (find-object x) (find-object *player-1*)))
(defun player-2-p (x) (eq (find-object x) (find-object *player-2*)))

(defun find-ships ()
  (find-instances (arena) 'ship))

(defun ball-carrier ()
  (find-if #'carrying-ball-p (find-ships)))

;;; Song clock

(defvar *song-clock* 0)

(defun reset-song-clock ()
  (setf *song-clock* (* 60 *current-song-length*)))

(defun update-song-clock ()
  (when (plusp *song-clock*)
    (decf *song-clock*)))

(defun game-on-p () (plusp *song-clock*))

(defun song-clock () *song-clock*)

(defun song-clock-string (&optional (clock *song-clock*))
  (let ((minutes (/ clock (minutes 1)))
	(seconds (/ (rem clock (minutes 1)) 
		    (seconds 1))))
    (format nil "~D~A~2,'0D" (truncate minutes) ":" (truncate seconds))))

;;; Main program entry point

(defun danceball ()
  (with-session 
    (open-project "danceball")
    (initialize-screen)
    (index-all-images)
    (index-all-samples)
    (index-pending-resources)
    (preload-resources)
    (setf *default-texture-filter* :linear)
    (setf *dj-use-voice-p* nil)
    ;; preload menu music for now
    (mapc #'xelf:find-resource (mapcar #'song-ogg *level-songs*))
    (initialize-sounds)
    (disable-key-repeat) 
    (make-instance 'dj)
    (load-configuration (dj))
    ;;(show-menu (dj))))
    (play-current-mode (dj) "nightclubbing")))


