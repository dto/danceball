(in-package :danceball)

(defmethod draw ((self thing))
  (with-slots (color image heading) self
    (multiple-value-bind (top left right bottom)
	(bounding-box self)
      (draw-textured-rectangle-* left top 0
				 (- right left) (- bottom top)
				 (find-texture image)
				 ;; apply shading
	                         :vertex-color color
			         :window-x 0
                                 :window-y 0
				 :blend :alpha
				 ;; adjust angle to normalize for up-pointing sprites 
				 :angle (+ 90 (heading-degrees heading))))))

(defmethod draw ((self grid-actor))
  (with-slots (x y image color heading) self
    ;; (if image
    ;;     (draw-textured-rectangle-* x y 0 *cell-width* *cell-height*
    ;;     			   (find-texture image)
    ;;     			   ;; apply shading
    ;;     			   :vertex-color color
    ;;     			   :blend :alpha
    ;;                                :window-x 0
    ;;                                :window-y 0
    ;;     			   ;; adjust angle to normalize for up-pointing sprites 
    ;;     			   :angle (+ 90 (heading-degrees heading)))
    ;;     (progn 
	  (draw-frame
	   (or (current-animation-frame self)
	       (default-animation-frame self))
	   x y
	   *cell-width*
	   *cell-height*
           (+ 90 (heading-degrees heading))
           color
           )
          ;; draw glow
          (draw-frame (find-glow-frame (or (current-animation-frame self)
                                           (default-animation-frame self)))
                      (- x 2) (- y 2)
                      (+ *cell-width* 3)
                      (+ *cell-height* 3)
                      (+ 90 (heading-degrees heading))
                      color)
          
        
    ;; draw border if any
    (when (half-beat-shielded-p self)
      (draw-frame
       (current-shield-animation-frame self)
       x y
       *cell-width*
       *cell-height*
       (+ 90 (heading-degrees heading))
       (random-choose '("cyan" "white"))))))

(defmethod draw :after ((ball ball))
  (with-slots (color x y width height) ball
    (draw-textured-rectangle-* x y 0 width height (find-texture "ball-indicator.png")
                               :window-x 0
                               :window-y 0
                               :vertex-color color)))

(defmethod draw ((wall wall))
  (with-slots (x y width height color) wall
    (draw-textured-rectangle-* x y 0 width height (find-texture "white.png") :vertex-color color
                               :window-x 0 :window-y 0)))

(defmethod draw ((brick brick))
  (with-slots (x y width height color) brick
    (draw-textured-rectangle-* x y 0 width height (find-texture "white.png")
                               :window-x 0 :window-y 0
                               :vertex-color color)
    (let ((border-x (* 0.20 width))
          (border-y (* 0.20 height)))
      (draw-textured-rectangle-* (- x border-x)
                                 (- y border-y)
                                 0
                                 (+ width (* border-x 2))
                                 (+ height (* border-y 2))
                                 (find-texture "brick-glow.png")
                                 :window-x 0
                                 :window-y 0
                                 :vertex-color color
                                 :opacity 0.8))))

(defmethod draw :around ((arena arena))
  (if *zoom*
      (let ((*gl-screen-height* (* *gl-screen-height* 8))
            (*gl-screen-width* (* *gl-screen-width* 8)))
        (call-next-method))
      (call-next-method)))

;; (defmethod draw ((arena arena))
;;   (xelf::project-window arena)
;;   (multiple-value-bind (top left right bottom)
;;           (window-bounding-box arena)
;;     (loop for object being the hash-keys of (slot-value arena 'objects) do
;;       ;; only draw onscreen objects
;;       (when (colliding-with-bounding-box-p (find-object object) top left right bottom)
;; 	(draw (find-object object))))))
    
(defmethod draw ((arena arena))
  (xelf::project-window arena)
  (draw-gradient arena)
  (draw-grid-texture arena)
  (multiple-value-bind (top left right bottom)
      (if *zoom*
          (bounding-box arena)
          (window-bounding-box arena))
    (loop for object being the hash-keys of (slot-value arena 'objects) do
      ;; only draw onscreen objects
      (when (colliding-with-bounding-box-p (find-object object) top left right bottom)
	(draw (find-object object))))
    (when (player-1)
      (draw-string (format nil "dance meter:                                 title: ~S    ~A BPM" (title (dj)) (bpm (dj)))
                   (+ 10 left (xelf::window-origin-x))
                   (+ 10 top (xelf::window-origin-y))
                   :color "yellow"
                   :font "sideways")
      (draw-string (prin1-to-string (compute-score (dj) arena))
                   (+ -150 right (xelf::window-origin-x))
                   (+ 100 top (xelf::window-origin-y))
                   :color "white"
                   :font "sideways")
      (draw-string (song-clock-string)
                   (+ -150 right (xelf::window-origin-x))
                   (+ 20 top (xelf::window-origin-y))
                   :color "white"
                   :font "sideways-huge")
      (draw-string (format nil "difficulty: ~A" *difficulty*)
                   (+ 850 left (xelf::window-origin-x))
                   (+ 10 top (xelf::window-origin-y))
                   :color "yellow"
                   :font "sideways")
      (when (find-star)
        (multiple-value-bind (x y) (star-indicator-position)
          (draw-string "*" (+ x (xelf::window-origin-x))
                       (+ y (xelf::window-origin-y))
                       :color (random-choose '("black" "white")))))
      (when (and *message-string*
                 (message-should-display-p))
        (draw-string *message-string*
                     (+ left 100 (xelf::window-origin-x))
                     (+ top 100 (xelf::window-origin-y))
                     :color (random-choose '("white" "cyan" "yellow"))
                     :font "sideways-large"))
      (draw-textured-rectangle-* (+ 140 left (xelf::window-origin-x))
                                 (+ 8 top (xelf::window-origin-y))
                                 0
                                 200
                                 20 (find-texture "white.png")
                                 :vertex-color "gray30")
      (draw-textured-rectangle-* (+ 140 left (xelf::window-origin-x))
                                 (+ 8 top (xelf::window-origin-y))
                                 0
                                 (max 0 (* 2 (health-points (player-1))))
                                 20 (find-texture "white.png")
                                 :vertex-color (if (< (health-points (player-1)) 20)
                                                   (random-choose '("red" "yellow" "cyan"))
                                                   "green")))))
  
(defmethod draw-gradient ((arena arena))
  (draw-textured-rectangle-* (xelf::window-origin-x)
                             (xelf::window-origin-y)
                             0
                             *gl-screen-width*
                             *gl-screen-height*
                             (find-texture (background-image arena))
                             :window-x 0
                             :window-y 0))

(defmethod current-animation-frame ((arena arena))
  (let ((frames (spritesheet-animation-frames *spritesheet* (getf *animations* :grid-dance))))
    (nth (mod (current-dance-frame-index) 8) frames)))

(defmethod draw-grid-texture ((arena arena))
  (let* ((u (units 1))
         (r0 (1- (truncate (/ (xelf::window-origin-y) u))))
         (c0 (1- (truncate (/ (xelf::window-origin-x) u))))
         (maxrow (truncate (/ (height arena) u)))
         (maxcol (truncate (/ (width arena) u)))
         (rows (truncate (/ *height* u)))
         (columns (truncate (/ *width* u))))
    (dotimes (j (+ rows 2))
      (dotimes (i (+ columns 2))
        (when (and (<=  j maxrow)
                   (<=  i maxcol))
          (draw-frame (current-animation-frame arena)
                      (* (+ i c0) u)
                      (* (+ j r0) u)
                      u
                      u
                      nil
                      "gray13"))))))

(defmethod draw-card ((arena arena))
  (let ((x (xelf::window-origin-x))
        (y (xelf::window-origin-y))
        (factor (card-scale-factor arena))
        (image (card-image arena)))
    (multiple-value-bind (top left right bottom) 
        (xelf:scale-bounding-box (list y
                                       x
                                       (+ x *gl-screen-width*)
                                       (+ y *gl-screen-height*))
                                 factor)
      (draw-textured-rectangle-* left top
                                 0
                                 (- right left)
                                 (- bottom top)
                                 (find-texture image)
                                 :window-x 0
                                 :window-y 0))))

(defmethod draw-card :around ((arena arena))
  (when (card-showing-p arena)
    (call-next-method)))

;;; Add a faux-CRT phosphor effect to the whole scene

(defmethod draw :after ((arena arena))
  (draw-card arena)
  (draw-textured-rectangle-* (xelf::window-origin-x)
                             (xelf::window-origin-y)
                             0
                             *gl-screen-width*
                             *gl-screen-height*
                             (find-texture "phosphor3.png")
                             :blend :multiply
                             :window-x 0
                             :window-y 0))

(defmethod draw :after ((self ship))
  (with-slots (x y color heading kick-clock waypoints shield-clock shield-orientation) self
    (draw-frame (current-animation-frame self)
                x y *cell-width* *cell-height*
                (+ 90 (heading-degrees heading))
                "white")
    (draw-frame (current-indicator-frame self)
                x y *cell-width* *cell-height*
                (+ 90 (heading-degrees heading))
                (if (string= color "white")
                    "black"
                    color))
    ;; shield
    (case shield-orientation
      (:horizontal (draw-frame (shield-horizontal-frame)
                               x y *cell-width* *cell-height*
                               (+ 90 (heading-degrees heading))
                               (random-choose '("cyan" "magenta" "yellow"))))
      (:vertical (draw-frame (shield-vertical-frame)
                               x y *cell-width* *cell-height*
                               (+ 90 (heading-degrees heading))
                               (random-choose '("cyan" "magenta" "yellow")))))))

(defmethod draw ((gate gate))
  (with-slots (x y width height color) gate
    (draw-textured-rectangle-* x y 0 width height (find-texture "gate.png")
                               :window-x 0 :window-y 0
                               :vertex-color color)
    (let ((border-x (* 0.085 width))
          (border-y (* 0.085 height)))
      (draw-textured-rectangle-* (- x border-x)
                                 (- y border-y)
                                 0
                                 (+ width (* border-x 2))
                                 (+ height (* border-y 2))
                                 (find-texture "gate-glow.png")
                                 :window-x 0
                                 :window-y 0
                                 :vertex-color color
                                 :opacity 0.8))))

(defmethod draw ((glow glow))
  (with-slots (x y clock) glow
    (draw-textured-rectangle-*
     (- x 100 (random 15)) (- y 100 (random 15)) 0 200 200 (find-texture "hoop.png")
     :window-x 0  
     :window-y 0
     :blend :additive
     :vertex-color (list 100 100 255)))
  (set-blending-mode :alpha))

(defmethod draw ((spark spark))
  (set-blending-mode :additive)
  (with-slots (x y clock) spark
    (dotimes (n 2)
      (let ((z (+ 2 (random 4))))
	(draw-textured-rectangle-*
	 (+ x (random 20) (xelf::window-origin-x))
	 (+ y (random 20) (xelf::window-origin-y))
	 0 20 20
	 (find-texture (random-choose *rez-lights*))
	 :blend :additive
	 :opacity 0.95)))
  (set-blending-mode :alpha)))

(defmethod draw :after ((arrow arrow))
  (let ((color (if (activated-p arrow)
                   "magenta"
                   "yellow green")))
    (with-slots (direction hold-length) arrow
      (multiple-value-bind (cx cy) (center-point arrow)
        (multiple-value-bind (px py) (step-in-direction cx cy direction (units 0.5))
          (cond
            ((or (eq direction :left)
                 (eq direction :right))
             (draw-textured-rectangle-* px (xelf::window-origin-y) 0 2 *screen-height* (find-texture "white.png")
                                        :window-x 0 :window-y 0 :vertex-color (random-choose '("yellow" "green")))
             (cond ((eq direction :left)
                    (draw-textured-rectangle-* px (xelf::window-origin-y) 0
                                               (units hold-length) *screen-height* (find-texture "translucent.png")
                                               :window-x 0 :window-y 0 :opacity 0.1 :vertex-color color))
                   ((eq direction :right)
                    (draw-textured-rectangle-* (- px (units hold-length)) (xelf::window-origin-y) 0
                                               (units hold-length) *screen-height* (find-texture "translucent.png")
                                               :window-x 0 :window-y 0 :opacity 0.1 :vertex-color color))))
            ((or (eq direction :up)
                 (eq direction :down))
             (draw-textured-rectangle-* (xelf::window-origin-x) py 0 *screen-width* 2 (find-texture "white.png")
                                        :window-x 0 :window-y 0 :vertex-color (random-choose '("yellow" "green")))
             (cond ((eq direction :down)
                    (draw-textured-rectangle-* (xelf::window-origin-x) (- py (units hold-length)) 0
                                               *screen-width* (units hold-length) (find-texture "translucent.png")
                                               :window-x 0 :window-y 0 :opacity 0.1 :vertex-color color))
                   ((eq direction :up)
                    (draw-textured-rectangle-* (xelf::window-origin-x) py 0
                                               *screen-width* (units hold-length) (find-texture "translucent.png")
                                               :window-x 0 :window-y 0 :opacity 0.1 :vertex-color color))))))))))

(defmethod draw ((chevron chevron))
  (with-slots (x y width height color image direction) chevron
    (draw-textured-rectangle-* x y 0 width height (find-texture image)
                               :window-x 0 :window-y 0
                               :vertex-color color)))

(defmethod draw ((ghost ghost))
  (with-slots (color image heading attack-clock) ghost
    (multiple-value-bind (top left right bottom)
	(bounding-box ghost)
      (draw-textured-rectangle-* left top 0
				 (- right left) (- bottom top)
				 (find-texture image)
			         :window-x 0
                                 :window-y 0)
      (when (plusp attack-clock)
        (let ((radius (attack-radius ghost)))
          (draw-frame (attack-animation-frame ghost)
                      (- left radius (units -0.5))
                      (- top radius (units -0.5))
                      (* radius 2)
                      (* radius 2)
                      0
                      (random-choose '("magenta" "yellow" "cyan"))))))))
