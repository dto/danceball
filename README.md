
Danceball is a rhythm game written in the Common Lisp programming
language. For build instructions, check out the INSTALL file. To
learn more about the game and find prebuilt binaries, check out the
official page: https://dto.itch.io/danceball/
