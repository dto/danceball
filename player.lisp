(in-package :danceball)

;;; Player ship properties

(defparameter *max-speed* 2.4)
(defparameter *max-carry-speed* 2.3)

(defclass ship (actor)
  ((color :initform "black")
   (carrying :initform t)
   (kick-clock :initform 0)
   (boost-clock :initform 0)
   (shield-orientation :initform nil)
   (shield-clock :initform 0)
   (static-frame :initform (single-frame *spritesheet* 1 1))))

(defmethod shield-p ((ship ship))
  (slot-value ship 'shield-orientation))

(defmethod update-shield-clock ((ship ship))
  (with-slots (shield-clock) ship
    (decf shield-clock)
    (when (zerop shield-clock)
      (setf (slot-value ship 'shield-orientation)
            nil))))

(defmethod update-boost-clock ((ship ship))
  (with-slots (boost-clock) ship
    (when (plusp boost-clock)
      (decf boost-clock))))

(defmethod raise-shield ((ship ship) direction)
  (when (not (shield-p ship))
    (play-sound ship "newball.wav")
    (setf (slot-value ship 'shield-orientation) direction)
    (setf (slot-value ship 'shield-clock) (truncate (* 60.0 2.0 (seconds-per-beat))))))

(defmethod humanp ((ship ship)) nil)

(defparameter *ship-reload-frames* 30)

;;; Utilities

(defmethod can-reach-ball ((ship ship))
  (and (ball) (colliding-with-p ship (ball))))

(defmethod ball-centered-p ((ship ship))
  (> 0.4 (abs (wobble))))

(defparameter *ship-shoot-distance* 680)

(defmethod ready-to-kick-p ((ship ship)) 
  (zerop (slot-value ship 'kick-clock)))

(defparameter *kick-speed* 25)
(defparameter *steal-speed* 22)
(defparameter *kick-range* (units 2.8))

(defmethod ball-within-range-p ((ship ship))
  (< (distance-between ship (ball))
     *kick-range*))

(defparameter *repel-range* (units 4))

(defmethod opponent-within-range-p ((ship ship))
  (< (distance-between ship (opponent ship))
     *repel-range*))

(defmethod opponent-carrying-p ((ship ship))
  (with-slots (carrying) ship
    (and (not carrying)
	 (ball-carrier))))

(defun both-joysticks-connected ()
  (numberp *player-2-joystick*))

(defmethod stick-heading ((self ship)) nil)

(defmethod heading-to-opponent ((ship ship))
  (heading-between ship (opponent ship)))

(defmethod heading-to-ball ((ship ship))
  (if (ball) (heading-between ship (ball)) 0))

(defmethod distance-to-opponent ((ship ship))
  (distance-between ship (opponent ship)))

(defmethod distance-to-ball ((ship ship))
  (if (ball) (distance-between ship (ball)) 10000))

;;; Kicking the Danceball

(defmethod kick ((self ship))
  (with-slots (heading carrying kick-clock color) self
    (play-sound self (random-choose *bounce-sounds*))
    (setf kick-clock *ship-reload-frames*)
    (when carrying
      (multiple-value-bind (x y) (carry-location self)
        (lose-ball self)
        (insert (make-instance 'ball :color color))
        (move-to (ball) x y)
        (impel (ball) :heading heading :speed 10)
        (when (only-on-half-beat-p)
          (setf (fired-on-half-beat-p (ball)) t))
	(play-sound self "serve.wav")))))

;;; Friction modification. This section is obsolete.

(defun decay-less (x)
  (let ((z (* 0.8 x)))
    z))

(defmethod decay ((self ship) value)
  (decay-less value))

;;; Scoring and goals

(defmethod find-score ((ship ship)) (slot-value ship 'score))

(defmethod find-goal ((ship ship)) (goal-1))

(defmethod carrying-ball-p ((ship ship))
  (slot-value ship 'carrying))

(defmethod max-speed ((ship ship))
  (if (carrying-ball-p ship) 
      *max-carry-speed* 
      (slot-value ship 'max-dx)))

;;; Updating the ship 

(defmethod update ((ship ship))
  (with-slots (kick-clock) ship
    (when (plusp kick-clock)
      (decf kick-clock))
    (update-shield-clock ship)
    (update-boost-clock ship)
    (when (and (ready-to-kick-p ship)
	       ;; whether to allow spamming the ball when you don't have it
	       ;; (ball-carrier)
	       (kicking-p ship))
      (kick ship))))

;;; Ships repel each other 

(defmethod collide ((this ship) (that ship))
  (repel this that))

;;; Ships can't pass through walls or bricks

(defmethod collide ((wall wall) (ship ship))
  (cancel-slide ship)
  (impel ship :speed 10 :heading (heading-between wall ship)))

(defmethod collide ((brick brick) (ship ship))
  (cancel-slide ship)
  (impel ship :speed 10 :heading (heading-between brick ship)))

;;; Ships can grab and lose the ball

(defmethod lose-ball ((ship ship))
  (with-slots (carrying) ship
    (setf carrying nil)))

(defmethod grab ((ship ship))
  (when (not (recently-kicked-p (ball)))
    (paint ship (color (ball)))
    (destroy (ball))
    (play-sound ship (random-choose *doorbell-sounds*))
    (with-slots (carrying) ship
      (setf carrying t))))

;;; Ball snaps to position when player is carrying

(defun wobble () (sin (/ xelf:*updates* 10)))

(defmethod carry-location ((ship ship))
  (with-slots (heading) ship
    (multiple-value-bind (cx cy) (center-point ship)
      (multiple-value-bind (tx ty) 
	  (step-coordinates cx cy heading (units 1))
	  (values (- tx (units 0.5)) (- ty (units 0.5)))))))

;;; PLAYER-1 basic definitions

(defclass player-1 (ship) 
  ((color :initform "white")
   (damage-animation :initform :player-damage)
   (maximum-health :initform 100)
   (player-id :initform 1)))

(defmethod humanp ((self player-1)) t)

(defmethod find-goal ((self player-1))
  (goal-2))

(defmethod think ((player-1 player-1))
  (let ((dir (currently-pressed-direction)))
    (when dir
      (face player-1 dir)
      (slide-on-grid player-1 dir))
    (when (kicking-p player-1)
      (kick player-1))))

(defmethod movement-heading ((player-1 player-1))
  nil)

;;; PLAYER-2 basic definitions

(defclass player-2 (ship) 
  ((color :initform *player-2-color*)
   (player-id :initform 2)))

(defmethod humanp ((self player-2))
  (not (null *player-2-joystick*)))

(defmethod find-goal ((ship player-2)) 
  (goal-1))

;;; Player 1 input control

(defun currently-pressed-direction ()
  (when *player-1-joystick*
    (when (left-analog-stick-pressed-p *player-1-joystick*)
      (heading-direction (left-analog-stick-heading *player-1-joystick*)))))
  ;; (cond ((button-recently-pressed-p :up) :up)
  ;;       ((button-recently-pressed-p :down) :down)
  ;;       ((button-recently-pressed-p :left) :left)
  ;;       ((button-recently-pressed-p :right) :right)))

(defun currently-pressed-directions ()
  (delete nil (list (when (button-recently-pressed-p :up) :up)
                    (when (button-recently-pressed-p :down) :down)
                    (when (button-recently-pressed-p :left) :left)
                    (when (button-recently-pressed-p :right) :right))))

(defmethod stick-heading ((self player-1))
  (let ((dir (currently-pressed-direction)))
    (when dir (direction-heading dir))))
  
  ;; (let ((dirs (currently-pressed-directions)))
  ;;    (if (> (length dirs) 1)
  ;;        (prog1 nil
  ;;          ;; don't slide when raising shields
  ;;          (when (sliding-p self)
  ;;            (cancel-slide self))
  ;;          (cond
  ;;            ((equal dirs '(:up :down))
  ;;             (raise-shield self :vertical))
  ;;            ((equal dirs '(:left :right))
  ;;             (raise-shield self :horizontal))))
  ;;        (if (and *player-1-joystick*
  ;;                 (currently-pressed-direction))
  ;;            (direction-heading (currently-pressed-direction))
  ;;            (call-next-method)))))

(defmethod movement-heading ((self player-1))
  (stick-heading self))

(defmethod kicking-p ((ship player-1))
  (or (button-recently-pressed-p :cross)
      (button-recently-pressed-p :circle)))

(defmethod update :before ((self player-1))
  (stick-heading self))

;;; Player 2 AI/input control

(defmethod stick-heading ((self player-2))
  (if *player-2-joystick*
      (when (left-analog-stick-pressed-p *player-2-joystick*)
	(left-analog-stick-heading *player-2-joystick*))
      (progn
	(path-heading self))))

(defmethod kicking-p ((ship ship))
  (cond ((and (both-joysticks-connected)
	      (not *netplay*))
	 (holding-button-p *player-2-joystick*))
	((not (game-on-p)) nil)
	((colliding-with-p (ball) (goal-1)) nil)
	((carrying-ball-p ship)
	 (percent-of-time 
	     (if (< (distance-between ship (find-goal ship)) 240)
		 (if (not (difficult-p)) 15 19)
		 (if (not (difficult-p)) 17 20))
	   (and (ball-centered-p ship)
		(< (distance-between ship (find-goal ship)) 
		   (if (not (difficult-p)) 
		       (+ *ship-shoot-distance* 30)
		       (+ *ship-shoot-distance* 70))))))
	((carrying-ball-p (opponent ship))
	 (percent-of-time (if (not (difficult-p)) 0.9 1.3)
	   (and 
	    (opponent-within-range-p ship)
	    (ball-within-range-p ship))))))

;;; Steering behaviors

;; In order to steer somewhere we must have a target. When the AI has the
;; ball, it seeks out the opponent's goal; when it doesn't have the ball,
;; it seeks the ball.

(defparameter *target-margin* (units 4))

(defmethod find-ball-target ((ship ship))
  (multiple-value-bind (cx cy) (center-point (arena))
    (let ((rx (center-point ship))
	  (bx (center-point (ball)))
	  (left (- cx *target-margin* (units 2)))
	  (right (+ cx *target-margin* (units 2)))
	  (b1x (center-point (barrier-1)))
	  (b2x (center-point (barrier-2))))
      (if (and (zonep (ball))
	       (slow-p (ball)))
	  (cond ((< (abs (- rx b1x))
		    (abs (- rx bx)))
		 (values (- cx *target-margin*) cy))		       
		((< (abs (- rx b2x))
		    (abs (- rx bx)))
		 (values (+ cx *target-margin*) cy))
		(t (center-point (ball))))
	  (center-point (ball))))))

(defmethod target ((ship ship))
  (multiple-value-bind (ax ay) (center-point (arena))
    (if (not (carrying-ball-p ship))
	(center-point (ball))
	(center-point (find-goal ship)))))

(defun jitter (heading)
  (+ heading (* (if (difficult-p) 0.5 0.1) (sin (/ *updates* 24)))))

(defun course-correction ()
  (if (difficult-p) 
      (if (serve-period-p) 0.3 0.2)
      (if (serve-period-p) 0.33 0.22)))

;; This method needs to be refactored; it controls how aggressively (and
;; how accurately) the bot steers. A future version of this document will
;; feature a refactored version. 

(defmethod movement-heading ((ship ship))
  (if (or *netplay* (both-joysticks-connected))
      (stick-heading ship)
      (when (and (game-on-p)
		 (not (either-goal-flashing-p)))
	(percent-of-time
	    (if (serve-period-p)
		(if (not (difficult-p)) 55 67)
		(if (and (zonep (ball))
			 (slow-p (ball)))
		    ;; anticipate eject but don't superspeed
		    (if (not (difficult-p)) 50 65)
		    (if (and 
			 (not (carrying-ball-p ship))
			 (at-rest-p (ball))
			 (ball-within-range-p ship))
			;; slow down to catch ball
			(if (slow-p ship) 60 65)
			;; default 
			(if (not (difficult-p)) 77 88))))
	  (when (and (not (colliding-with-p (ball) (goal-1))) 
		     (not (colliding-with-p (ball) (goal-2))))
	    ;; follow pathfinding nodes whenever possible
	    (or (path-heading ship)
		(multiple-value-bind (cx cy) (center-point ship)
		  (multiple-value-bind (wx wy) (target ship)
		    (if (carrying-ball-p ship)
			(jitter (find-heading cx cy wx wy))
			(if (fast-p ship)
			    ;; correct path to not overshoot ball
			    (let ((delta (- (find-heading cx cy wx wy)
					    (trajectory-heading ship))))
			      (if (plusp delta)
				  (jitter (+ (find-heading cx cy wx wy) (course-correction)))
				  (jitter (- (find-heading cx cy wx wy) (course-correction)))))
			    (jitter (find-heading cx cy wx wy))))))))))))

;;; Sensing the environment with "feelers"

;; By using the Xelf quadtree facility to peek at a series of points
;; along a line, we can see if a wall or other obstacle is nearby. Notice
;; that the OBSTACLE-P slot is checked here.

(defmethod can-see-point-p ((self ship) x y)
  (block colliding
    (multiple-value-bind (x0 y0) (center-point self)
      (let ((d (/ (distance x0 y0 x y) 30))
	    (w 0)
	    (h (find-heading x0 y0 x y)))
	(dotimes (n 30)
	  (incf w d)
	  (multiple-value-bind (x1 y1)
	      (step-toward-heading self h w)
	    (let* ((vtop (- y1 1))
		   (vleft (- x1 1))
		   (vright (+ vleft 2))
		   (vbottom (+ vtop 2)))
	      (flet ((check (object)
		       (when (obstacle-p object)
			 (return-from colliding nil))))
		(prog1 t
		  (xelf::quadtree-map-collisions *quadtree*
						 (cfloat vtop)
						 (cfloat vleft)
						 (cfloat vright)
						 (cfloat vbottom)
						 #'check))))))
	(return-from colliding t)))))

;;; Now we can use these "feelers" like cat whiskers to detect walls on
;; either side of the current trajectory.

(defmethod heading-to-waypoint ((self ship))
  (multiple-value-bind (cx cy) (center-point self)
    (with-slots (goal-x goal-y heading) self
      (if (null goal-x)
	  heading
	  (find-heading cx cy goal-x goal-y)))))

(defmethod facing-wall-p ((self ship))
  (with-slots (heading) self
    (multiple-value-bind (cx cy) (center-point self)
      (multiple-value-bind (px py)
	  (step-coordinates cx cy heading 60)
	(not (can-see-point-p self px py))))))

(defmethod facing-wall-left-p ((self ship))
  (with-slots (heading) self
    (multiple-value-bind (cx cy) (center-point self)
      (multiple-value-bind (px py)
	  (step-coordinates cx cy (+ heading 0.3) 60)
	(not (can-see-point-p self px py))))))

(defmethod facing-wall-right-p ((self ship))
  (with-slots (heading) self
    (multiple-value-bind (cx cy) (center-point self)
      (multiple-value-bind (px py)
	  (step-coordinates cx cy (- heading 0.3) 60)
	(not (can-see-point-p self px py))))))

;;; When these feelers are triggered, we steer away. 

(defmethod update :before ((self player-2))
  ;; re-path if needed
  (with-slots (goal-x goal-y) self
    (when (or (null goal-x) (zerop (mod *updates* 10)))
      (multiple-value-bind (x y) (target self)
	(setf goal-x nil goal-y nil)
	(seek-to self x y))))
  ;; steer away from walls 
  (with-slots (heading) self
    (if (facing-wall-left-p self)
	(progn ;; steer right
	  (next-waypoint self)
	  (incf heading -0.0001))
	(if (facing-wall-right-p self)
	    (progn ;; steer left
	      (next-waypoint self)
	      (incf heading 0.0001))))))


;; The steering AI will need to be able to find the current waypoint.

(defmethod path-target ((self thing))
  (with-slots (goal-x goal-y) self
    (if goal-x
	(values goal-x goal-y)
	(progn
	  (multiple-value-bind (x y) (target self)
	    (seek-to self x y))
	  (next-waypoint self)))))

(defmethod path-heading ((self thing))
  (with-slots (x y goal-x goal-y waypoints) self
    (when (and goal-x goal-y)
      (if (< 10 (distance x y goal-x goal-y))
	  ;; keep walking 
	  (find-heading x y goal-x goal-y)
	  (if waypoints
	      (find-heading x y goal-x goal-y)
	      (setf goal-x nil goal-y nil))))))

;;; Pathfinding for Player 1 is turned off.

(defmethod seek-to ((self player-1) x y) nil)

;;; Pathfinding obstruction definitions

;;(defmethod will-obstruct-p ((bumper bumper) (ship ship)) t)
(defmethod will-obstruct-p ((p1 player-1) (p2 player-2)) t)
(defmethod will-obstruct-p ((p1 player-2) (p2 player-1)) t)

;;; Animating the ship in its different orientations and colors

(defmethod frames-for-direction ((ship ship))
  (let ((key (case (slot-value ship 'direction)
               (:up :player-dance-up)
               (:down :player-dance-down)
               (:left :player-dance-left)
               (:right :player-dance-right)
               (otherwise :player-dance-up))))
    (let ((animation (getf *animations* key)))
      (spritesheet-animation-frames *spritesheet* animation))))

(defmethod indicator-frames-for-direction ((ship ship))
  (let ((key (case (slot-value ship 'direction)
               (:up :player-color-up)
               (:down :player-color-down)
               (:left :player-color-left)
               (:right :player-color-right)
               (otherwise :player-color-up))))
    (let ((animation (getf *animations* key)))
      (spritesheet-animation-frames *spritesheet* animation))))

(defmethod current-animation-frame ((ship ship))
  (let ((frames (frames-for-direction ship)))
    (nth (mod (current-dance-frame-index) 8) frames)))

(defmethod current-indicator-frame ((ship ship))
  (let ((frames (indicator-frames-for-direction ship)))
    (nth (mod (current-dance-frame-index) 8) frames)))

(defmethod collide ((ship ship) (bullet bullet))
  (if (not (shield-p ship))
      (progn (repel bullet ship)
             (damage ship 25))
      (cond ((and (eq :horizontal (slot-value ship 'shield-orientation))
                  (or (eq :up (direction bullet))
                      (eq :down (direction bullet))))
             (damage ship 25))
            ((and (eq :vertical (slot-value ship 'shield-orientation))
                  (or (eq :right (direction bullet))
                      (eq :left (direction bullet))))
             (damage ship 25))
            (t
             (incf (health-points ship) (with-difficulty 25 15 8))
             (display-message "ABSORB!")
             (play-sound ship "block.wav"))))
  (destroy bullet))

(defun random-bip-sound () (random-choose *bip-sounds*))

(defmethod collide ((ship ship) (enemy enemy))
  (damage ship (with-difficulty* 20 30 40))
  (make-sparks (+ (x enemy) (units 0.5))
               (+ (y enemy) (units 0.5)))
  (damage enemy 3))

(defmethod die :before ((ship ship))
  (fail (dj))
  (play-sample "explosion-1.wav")
  (make-sparks (+ (x ship) (units 0.5))
               (+ (y ship) (units 0.5))
               "magenta"))

(defmethod arrive :after ((ship ship))
  (setf (health-points ship)
        (min (maximum-health ship)
             (+ (with-difficulty 1.3 0.9 0.6)
                (health-points ship)))))

(defmethod collide :around ((ship ship) (thing thing))
  (when (will-obstruct-p thing ship)
    (cancel-slide ship)
    (impel ship :heading (heading-between thing ship) :speed 10))
  (call-next-method))

(defmethod collide ((ship ship) (star star))
  (make-sparks (+ (x star) (units 0.5))
               (+ (y star) (units 0.5)))
  (destroy star)
  (teach-say (dj) :got-star)
  (teach-say (dj) :now-survive)
  (play-sample "woop.wav"))

(defmethod update :before ((ship ship))
  (cond ((or (button-pressed-p *keyboard-controller* :left-bumper)
             (button-pressed-p *controller-1* :left-bumper))
         (raise-shield ship :horizontal))
        ((or (button-pressed-p *keyboard-controller* :right-bumper)
             (button-pressed-p *controller-1* :right-bumper))
         (raise-shield ship :vertical))))

(defmethod collide ((chevron chevron) (ship ship))
  (unless (sliding-p ship)
    (multiple-value-bind (gx gy)
        (step-in-direction (grid-x ship) (grid-y ship) (direction chevron) 1)
      (slide-to-grid ship gx gy :frames 3))))

(defmethod collide :after ((chevron chevron) (ship ship))
  (with-slots (boost-clock) ship
    (when (zerop boost-clock)
      (play-sample "boost.wav")
      (setf boost-clock 60))))

(defmethod collide ((bomb bomb) (ship ship))
  (award-extra-points (dj) 500)
  (destroy bomb)
  (destroy-all-onscreen-enemies))

(defmethod collide ((mine mine) (ship ship))
  (when (on-beat-p)
    (damage ship (with-difficulty* 2.5 4.8 7.2))))

;; (defmethod fuzz-to-grid :after ((ship ship))
;;   (update-camera (arena)))

;; (defmethod snap-to-grid :after ((ship ship))
;;   (update-camera (arena)))

(defmethod will-obstruct-p ((gate gate) (ship ship)) t)
(defmethod will-obstruct-p ((brick brick) (ship ship)) t)
(defmethod will-obstruct-p ((wall wall) (ship ship)) t)
