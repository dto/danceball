(in-package :danceball)

(defmethod menu-p ((buffer buffer))
  nil)

;;; Gameworld map properties

(defclass arena (xelf:buffer)
  ((resetting :initform nil)
   (card-clock :initform nil :accessor card-clock)
   (card-image :initform nil :accessor card-image)
   (song-ended-p :initform nil :accessor song-ended-p)
   (song-started-p :initform nil :accessor song-started-p)
   (background-image :initform (random-choose *gradient-images*))
   (quadtree-depth :initform 9)))

(defmethod handle-beat ((arena arena)) nil)

(defmethod cleared-p ((arena arena))
  (song-ended-p arena))

(defmethod card-showing-p ((arena arena))
  (and (numberp (card-clock arena))
       (plusp (card-clock arena))))

(defmethod card-frames ((arena arena))
  (seconds->frames 1.0))

(defmethod show-card ((arena arena) card-image)
  (setf (card-image arena) card-image)
  (setf (card-clock arena) (card-frames arena)))

(defmethod card-scale-factor ((arena arena))
  (max 1.0 (+ 1.0
              (/ (card-clock arena)
                 (/ (card-frames arena) 4.0)))))
 
(defmethod visit :after ((arena arena))
  (dolist (node (get-nodes arena))
    (deploy node)))

(defvar *zoom* nil)

(defmethod toggle-zoom ((arena arena))
  (setf *zoom* (if *zoom* nil t)))

(defvar *reset-clock* nil)

(defmethod quit-game ((arena arena))
  (quit))

;; center player onscreen
(defmethod update-camera ((arena arena))
  (when (player-1)
    (move-window-to arena
                    (- (x (player-1))
                       (units -1)
                       (/ *gl-screen-width* 2.0))
                    (- (y (player-1))
                       (units -1)
                       (/ *gl-screen-height* 2.0)))))

(defmethod update :after ((arena arena))
  (update-camera arena)
  (when (and (numberp (card-clock arena))
             (plusp (- (card-clock arena) 2)))
    (decf (card-clock arena)))
  (when (not (plusp (- *song-clock* (seconds->frames 3.0))))
    (when (null (slot-value arena 'song-ended-p))
      (setf (slot-value arena 'song-ended-p) t)
      (if (cleared-p (arena))
          (clear (dj))
          (fail (dj))))))

;;; Adding keybindings

(defmethod cycle-difficulty ((arena arena))
  (change-difficulty (dj)))

(defmethod initialize-instance :after ((arena arena) &key)
  (setf *arena* arena)
  (initialize-button-states)
  (generate arena)
  (bind-event arena '(:escape) 'exit-arena)
  (bind-event arena '(:d :control) 'cycle-difficulty)
  (bind-event arena '(:z :control) 'toggle-zoom)
  (when (null (quadtree arena))
    (install-quadtree arena))
  (setf *inhibit-splash-screen* t))

(defmethod exit-arena ((arena arena))
  (show-menu (dj)))

;;; Generating levels

(defmethod generate ((arena arena)) nil)

(defmethod generate :before ((arena arena))
  (when (null (quadtree arena))
    (install-quadtree arena)))

(defmethod generate :after ((arena arena))
  (set-player-1 (first (find-instances arena 'player-1))))

;;; Puzzle arena

(defclass puzzle-arena (arena) ())

(defmethod cleared-p ((arena puzzle-arena))
  (and (song-ended-p arena)
       (plusp (num-stars (dj)))))

(defmethod update :after ((arena puzzle-arena))
  (when (on-beat-p)
    (percent-of-time (with-difficulty* 2 3 4) 
      (add-spawner arena)))
  ;; add arrows 
  (when (and (on-beat-p)
             (not (zerop (elapsed-beats)))
             (zerop (mod (elapsed-beats) 16)))
    (let ((dir (random-choose '(:up :down :left :right))))
      (percent-of-time (with-difficulty* 2 3 4)
        (place-arrow (arena) dir (+ 2 (random 3)) 8))
      (percent-of-time (with-difficulty* 2 3 4)
        (place-arrow (arena) (random-choose (remove dir '(:up :down :left :right)))
                     (+ 2 (random 3)) 8)))))

(defmethod generate ((arena puzzle-arena))
  (set-theme (random-choose (mapcar #'first *three-brick-themes*)))
  (with-buffer arena
    (multiple-value-bind (x y) (center-point arena)
      (let ((puzzle (make-puzzle-3 (theme-colors))))
        (paste-from arena puzzle
                    (units 7) (units 7))
        (resize arena (+ (width puzzle) (units 18)) (+ (height puzzle) (units 18)))
        (paste-from arena (chevrons :right) (/ (width arena) 3) (units 2))
        (paste-from arena (chevrons :left) (/ (width arena) 3) (- (height arena) (units 8)))
        (paste-from arena (chevrons :up) (units 2) (/ (height arena) 3))
        (paste-from arena (chevrons :down) (- (width arena) (units 8)) (/ (height arena) 3))
        (paste-from arena (chevrons :right) (/ (width arena) 1.8) (units 2))
        (paste-from arena (chevrons :left) (/ (width arena) 1.8) (- (height arena) (units 8)))
        (paste-from arena (chevrons :up) (units 2) (/ (height arena) 1.8))
        (paste-from arena (chevrons :down) (- (width arena) (units 8)) (/ (height arena) 1.8))
        ;;(trim arena)
        (paste-from (current-buffer) 
		    (make-border 0 0 (+ (width (current-buffer)) (units 1)) (+ (height (current-buffer)) (units 1))))
        (trim arena)
        (fill-empty-spaces arena)
        (setf (slot-value (current-buffer) 'background-color) nil)))))

(defmethod spawn-point ((arena arena))
  (with-slots (height width) arena
    (multiple-value-bind (x y) (center-point (player-1))
      (destructuring-bind (fx fy) 
          (block finding
            (loop do (let ((dir (random-choose '(:up :down :left :right :upleft :upright :downleft :downright)))
                           (dist (random-choose '(170 180 200 210 250 270 290 320))))
                       (multiple-value-bind (nx ny) (step-in-direction x y dir dist)
                         (when (and (<= 0 nx width)
                                    (<= 0 ny height))
                           (return-from finding (list nx ny)))))))
        (values fx fy)))))

(defmethod add-spawner ((arena arena))
  (multiple-value-bind (x y) (spawn-point arena)
    (add-node arena (make-instance 'spawner) x y)))

;;; The puzzle

(defparameter *puzzle-border* (units 1.51))

(defun wrap (thing buffer)
  (multiple-value-bind (top left right bottom)
      (find-bounding-box (get-nodes buffer))
    (prog1 buffer
      (with-buffer buffer
	(add-node buffer thing 
		    (- left *puzzle-border*)
		    (- top *puzzle-border*))
	(resize thing 
		(+ (* 2 *puzzle-border*) (- right left))
		(+ (* 2 *puzzle-border*) (- bottom top)))
	(trim (current-buffer))))))

(defparameter *color-phase* 0)

(defun nth-color (n)
  (nth (mod (+ n *color-phase*) (length (theme-colors)))
       (theme-colors)))

;;; Making rows of breakable bricks

(defparameter *fat-brick-width* 2.0)
(defparameter *fat-brick-height* 1.3)

(defun make-fat-brick (&optional color)
  (let ((brick (make-brick 0 0 color)))
    (resize brick *brick-width* *brick-height*)
    (prog1 brick)))

(defun fat-brick-row (x y length color)
  (dotimes (n length)
    (let ((brick (make-fat-brick color)))
      (add-node (current-buffer) brick (+ x (* n *brick-width*)) y))))

(defun themed-row (x y length)
  (let ((y0 y))
    (dolist (color (level-colors))
      (fat-brick-row x y0 length color)
      (incf y0 *brick-height*))))

(defun themed-row-buffer (length)
  (with-new-buffer
    (themed-row 0 0 length)
    (trim (current-buffer))))

(defun super-fat-row (x y width height color)
  (let ((y0 y))
    (dotimes (n height)
      (fat-brick-row x y0 width color)
      (incf y0 *brick-height*))))

(defparameter *sideline-width* 2) 

(defun bricks (color &optional (size 6))
  (bordered
   (with-new-buffer
     (super-fat-row 0 0 
		    (+ 3 (random 5))
		    (- 6 *depth*)
		    color)
     (trim (current-buffer)))))

(defun derange (things)
  (let ((len (length things))
	(things2 (coerce things 'vector)))
    (dotimes (n len)
      (rotatef (aref things2 n)
	       (aref things2 (random len))))
    (coerce things2 'list)))

(defvar *exit* nil)

(defun make-exit (colors)
  (let ((*puzzle-border* 18))
    (cond
      ((null colors)
       (with-new-buffer (add-node (current-buffer) 
				  (setf *exit* (make-instance 'star)))))
      ((consp colors)
       (wrap (make-instance 'gate :color (first colors))
	     (make-exit (rest colors)))))))

(defvar *extra-padding* 0)
  
(defun with-hpadding (amount buffer)
  (with-slots (height width) buffer
    (with-new-buffer 
      (paste-into (current-buffer) buffer amount 0) 
      (resize (current-buffer)
	      (+ width amount *extra-padding*)
	      height))))

(defun with-vpadding (amount buffer)
  (with-slots (height width) buffer
    (with-new-buffer 
      (paste-into (current-buffer) buffer 0 amount) 
      (resize (current-buffer)
	      width
	      (+ height amount *extra-padding*)))))

(defun with-automatic-padding (buffer)
  (with-border (units 1)
    (let ((padding (+ (units 0.5) 
		      (random (units 1.5)))))
      (if (evenp *depth*)
	  (with-hpadding padding buffer)
	  (with-vpadding padding buffer)))))

(defun horizontally (a b)
  (percent-of-time 50 (rotatef a b))
  (xelf::compose-beside 
   (with-border 10 a)
   (with-border 10 b)))

(defun vertically (a b)
  (percent-of-time 50 (rotatef a b))
  (xelf::compose-below 
   (with-border 10 a)
   (with-border 10 b)))

(defun either-way (a b)
  (funcall (or (percent-of-time 50 #'horizontally) #'vertically)
	   a b))

(defun bordered (x) 
  (with-border *puzzle-border* x))

(defun singleton (x) 
  (bordered (with-new-buffer (add-node (current-buffer) x) (trim (current-buffer)))))

(defun gated (color buf) (wrap (make-instance 'gate :color color) buf))

(defun random-color () (random-choose (theme-colors)))

(defvar *required-color* nil)

(defmacro requiring (color &body forms)
  `(let ((*required-color* ,color)) ,@forms))

(defun hazard () 
  (singleton (make-hazard)))

(defun wildcard ()
  (singleton (make-instance 'chevron :direction (random-choose *grid-directions*))))

(defun make-wildcard () (make-instance 'biclops))

(defun make-hazard () (make-instance (random-choose '(mine mine mine ghost))))

(defun stacked-up (&rest things)
  (assert things)
  (if (= 1 (length things))
      (first things)
      (xelf::compose-below (first things) (apply #'stacked-up (rest things)))))

(defun lined-up (&rest things)
  (assert things)
  (if (= 1 (length things))
      (first things)
      (xelf::compose-beside (first things) (apply #'lined-up (rest things)))))

(defun stacked-up-randomly (&rest things)
  (bordered (apply #'funcall #'stacked-up (derange things))))

(defun lined-up-randomly (&rest things)
  (bordered (apply #'funcall #'lined-up (derange things))))

(defun randomly (&rest things)
  (apply #'funcall (or (percent-of-time 50 #'stacked-up-randomly)
		       #'lined-up-randomly) 
	 things))

(defun laid-out (&rest things)
  (let ((*depth* (1+ *depth*)))
    (assert things)
    (apply #'funcall (if (evenp *depth*)
			 #'stacked-up
			 #'lined-up)
	   things)))
  
(defun mixed-up (&rest things)
  (assert things)
  (let ((layout (if (evenp *depth*)
		    #'stacked-up-randomly
		    #'lined-up-randomly)))
    (let ((*depth* (1+ *depth*)))
      (apply #'funcall layout things))))

(defun mixed-down (&rest things)
  (assert things)
  (let ((layout (if (oddp *depth*)
		    #'stacked-up-randomly
		    #'lined-up-randomly)))
    (let ((*depth* (1+ *depth*)))
      (apply #'funcall layout things))))

(defun skewed (&rest things)
  (assert things)
  (apply #'funcall #'mixed-up (mapcar #'with-automatic-padding things)))

(defun make-wall* (width height)
  (let ((wall (make-instance 'wall)))
    (resize wall width height)
    wall))

(defun horizontal-bulkhead (width)		   
  (let ((u (/ width 5)))
    (with-new-buffer
      (add-node (current-buffer) (make-wall* (* 2 u) (units 1)) 0 0)
      (add-node (current-buffer) (make-wall* (* 2 u) (units 1)) (* 3 u) 0))))

(defun vertical-bulkhead (width)		   
  (let ((u (/ width 5)))
    (with-new-buffer
      (add-node (current-buffer) (make-wall* (units 1) (* 2 u)) 0 0)
      (add-node (current-buffer) (make-wall* (units 1) (* 2 u)) 0 (* 3 u)))))

(defun make-bulkhead-buffer (horizontal size)
  (if horizontal
      (horizontal-bulkhead size)
      (vertical-bulkhead size)))

(defun with-bulkheads (buffer &optional (horizontal (percent-of-time 50 t)))
  (trim buffer)
  (if horizontal
      (stacked-up (bordered (make-bulkhead-buffer t (width buffer)))
		  (bordered buffer) 
		  (bordered (make-bulkhead-buffer t (width buffer))))
      (lined-up (bordered (make-bulkhead-buffer nil (height buffer)))
		(bordered buffer)
		(bordered (make-bulkhead-buffer nil (height buffer))))))

(defun make-player ()
  (singleton (make-instance 'player-1)))

(defun puzzle-3-components (colors)
  (assert (= 3 (length colors)))
  (let ((key (random-choose colors))
        (*depth* (random 2))
        (loc (random 3)))
    (destructuring-bind (A B C) (derange colors)
      (list
         (mixed-down (bricks B) (bricks C) (hazard) (hazard))
         (mixed-down (mixed-up (bricks A)
                               (if (= loc 0)
                                   (make-player)
                                   (hazard))
                               (bricks B)
                               (mixed-down (hazard)
                                           (hazard)))
                     (mixed-up
                      (mixed-down (bricks B)
                                  (if (= loc 1)
                                      (make-player)
                                      (hazard))
                                  (bricks A))
	              (gated B (mixed-down 
	                        (hazard)
                                (hazard)
	                        (gated A
		                       (mixed-up (hazard)
                                                 (chevrons)
			                         (bricks A)))))
                      (requiring key
                        (mixed-down
                         (mixed-up (bricks B)
                                   (singleton (make-instance 'mine))
                                   (bricks C)
                                   (if (= loc 2)
                                       (make-player)
                                       (hazard))
                                   (chevrons))
                         (gated A
                                (mixed-down 
                                 (mixed-up (hazard) (bricks B) (bricks C))
                                 ;; (hazard)
                                 (bordered
                                  (mixed-up
                                   (chevrons)
                                   (gated A (bricks (or *required-color* B)))
                                   ;;(hazard)
                                   (bricks B)))
                                 (make-exit (derange (theme-colors))))))))
                     (gated B (mixed-down (bricks C)
                                          (hazard)
                                          (gated A (mixed-up (chevrons)
                                                             (gated C (bricks A))
                                                             (hazard)
                                                             (mixed-down (bricks B)
                                                                         (bricks C)
                                                                         (chevrons))))
                                          (mixed-up (bricks A)
                                                    (bricks B))
                                          (hazard)
                                          (hazard)
                                          (mixed-up (bricks B)
                                                   (bricks C)
                                                   (hazard)))))))))
                           


(defun make-puzzle-3 (colors)
  (apply #'randomly
	 (puzzle-3-components colors)))

(defun pad-to-window (buffer)
  (prog1 buffer
    (with-slots (width height) buffer
      (setf width (max width *screen-width*))
      (setf height (max height *screen-height*)))))

(defmethod bounding-box-empty-p ((arena arena) top left right bottom)
  (let (found)
    (xelf::quadtree-map-collisions (quadtree arena)
			           (cfloat top)
                                   (cfloat left)
                                   (cfloat right)
                                   (cfloat bottom)
			     #'(lambda (thing)
                                 (setf found t)))
    (not found)))

(defmethod fill-empty-spaces ((arena arena) &optional (frequency 100))
  (let* ((u (units 12))
         (w (truncate (/ (width arena) u)))
         (h (truncate (/ (height arena) u))))
    (dotimes (x w)
      (dotimes (y h)
        (when (bounding-box-empty-p arena
                                    (* y u)
                                    (* x u)
                                    (+ u (* x u))
                                    (+ u (* y u)))
          (percent-of-time frequency
            (paste-from arena
                        (bricks (random-choose (theme-colors)))
                        (* x u)
                        (* y u))))))))

;;; Challenge mode

(defclass challenge-arena (arena)
  ((current-challenge :initform nil :initarg :current-challenge :accessor current-challenge)
   (beats-remaining :initform 0 :initarg :beats-remaining :accessor beats-remaining)))

(defmethod beats-remaining-p ((arena challenge-arena))
  (plusp (beats-remaining arena)))

(defmethod beats-per-challenge ((arena arena)) 64)

(defmethod initialize-challenge ((arena challenge-arena) challenge)
  (setf (current-challenge arena) challenge)
  (setf (beats-remaining arena)
        (beats-per-challenge arena)))

(defmethod initialize-challenge :after ((arena challenge-arena) challenge)
  (when (and (announce-keyword challenge)
             (not (input-suspended-p (dj))))
    (say (dj) (announce-keyword challenge))))

(defmethod initialize-instance :after ((arena challenge-arena) &key)
  (next-challenge arena))

(defmethod next-challenge ((arena challenge-arena))
  (destroy-all-objects 'bullet)
  (destroy-all-objects 'mine)
  (destroy-all-objects 'enemy)
  (initialize-challenge arena (random-challenge)))

;; (defmethod update :after ((arena challenge-arena))
;;   (update-camera arena))

(defmethod handle-beat ((arena challenge-arena))
  (update-challenge arena))

(defmethod update-challenge ((arena challenge-arena))
  (decf (beats-remaining arena))
  (if (beats-remaining-p arena)
      (generate-beat (current-challenge arena))
      (next-challenge arena)))
    
(defmethod generate ((arena challenge-arena))
  (with-buffer arena
    (multiple-value-bind (x y) (center-point arena)
      (let ((player (make-instance 'player-1)))
        (place arena player 32 32)
        (fuzz-to-grid player)
        (resize arena (units 64) (units 64))
        (paste-from (current-buffer) 
		    (make-border 0 0 (+ (width (current-buffer)) (units 1)) (+ (height (current-buffer)) (units 1))))
        (trim arena)))))

;;; Challenge classes

(defclass challenge ()
  ((classes :initform nil :initarg :classes :accessor classes)
   (announce-keyword :initform nil :initarg :announce-keyword :accessor announce-keyword)))

(defmethod things-per-beat ((challenge challenge))
  (with-difficulty 1 2 3))

(defmethod generate-beat ((challenge challenge))
  (dotimes (n (things-per-beat challenge))
    (multiple-value-bind (x y) (spawn-point (arena))
      (let ((thing (make-instance (random-choose (classes challenge)))))
        (add-node (arena) thing x y)
        (fuzz-to-grid thing)
        (make-sparks x y)))))

(defmethod generate-beat :after ((challenge challenge))
  (play-sample "spawn.wav"))

(defmethod generate-beat :around ((challenge challenge))
  (when (zerop (mod (elapsed-beats) 8))
    (call-next-method)))

(defclass mines-challenge (challenge)
  ((classes :initform '(mine))
   (announce-keyword :initform :mines)))

(defmethod things-per-beat ((challenge mines-challenge))
  (with-difficulty 2 4 6))

(defmethod generate-beat :after ((challenge mines-challenge))
  (multiple-value-bind (x y) (spawn-point (arena))
    (add-node (arena) (make-instance 'star) x y)))

(defclass robots-challenge (challenge)
  ((classes :initform '(spawner spawner spawner spawner spawner bomb))
   (announce-keyword :initform :robots)))

(defclass ghosts-challenge (challenge)
  ((classes :initform '(biclops biclops biclops biclops ghost ghost bomb))
   (announce-keyword :initform :ghosts)))

(defclass holds-challenge (challenge)
  ((classes :initform '(robot-1))
   (announce-keyword :initform :holds)))

(defmethod hold-length ((challenge holds-challenge))
  (random-choose (with-difficulty '(1 2 3) '(1 2 3 4) '(2 3 4))))

(defmethod things-per-beat ((challenge holds-challenge))
  (random-choose (with-difficulty '(1 1 1 2) '(1 2 2) '(1 2 2 2))))

(defmethod generate-beat ((challenge holds-challenge))
  (let ((dir (random-choose '(:up :down :left :right))))
    (place-arrow (arena) dir (hold-length challenge) 8)
    (when (> (things-per-beat challenge) 1)
      (place-arrow (arena) (random-choose (remove dir '(:up :down :left :right)))
                   (hold-length challenge) 8))))

(defun random-challenge ()
  (make-instance (random-choose '(mines-challenge robots-challenge
                                  ghosts-challenge holds-challenge))))

;;; Match-3 arena

(defclass match-arena (arena) ())

(defmethod generate ((arena match-arena))
  (with-buffer arena
    (set-random-theme)
    (multiple-value-bind (x y) (center-point arena)
      (let ((player (make-instance 'player-1)))
        (place arena player 32 32)
        (fuzz-to-grid player)
        (resize arena (units 64) (units 64))
        (paste-from (current-buffer) 
		    (make-border 0 0 (+ (width (current-buffer)) (units 1)) (+ (height (current-buffer)) (units 1))))
        (trim arena)))))

(defmethod visit :after ((arena match-arena))
  (let ((robots (make-formation (random-choose *formations*) 10 10)))
    (dolist (robot robots)
      (add-node arena robot (x robot) (y robot))))
  (let ((robots (make-formation (random-choose *formations*) 20 20)))
    (dolist (robot robots)
      (add-node arena robot (x robot) (y robot)))))


