;;; voice.el --- story frontend to espeak

;; Copyright (C) 2013, 2021 by  David O'Toole

;; Author: David O'Toole <deeteeoh1138@gmail.com>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require 'cl-lib)
(eval-when-compile (require 'cl))

(defvar *speech-program* "espeak")

(defvar *speech-parameters* 
  '(:voice "-v"
    :volume "-a"
    :pitch "-p"
    :markup "-m"
    :speed "-s"
    :punctuation "--punct"
    :file "-w"))

(defvar speech-additional-parameters '("--stdin" "-l" "0" "-m"))

(defun make-speech-parameter (entry)
  (destructuring-bind (name . value) entry
    (let ((option (getf *speech-parameters* name)))
      (when option
	(list option 
	      (if (stringp (first value))
		  (first value)
		  (format "%S" (first value))))))))

(defun pairwise (x)
  (when (consp x)
    (cons (list (first x) (second x))
	  (pairwise (rest (rest x))))))

(defun make-speech-parameters (babel-params)
  (apply #'append 
	 (mapcar #'make-speech-parameter
		 (pairwise babel-params))))

(defun speech-command-string (params)
  (mapconcat 'identity 
	     (append (list *speech-program*)
		     speech-additional-parameters
		     (make-speech-parameters params))
	     " "))

(defun speech-file (params) 
  (getf params :file))

(defun speech-render (text params file)
  (let ((command
	  (speech-command-string 
	   (if (null file) 
	       params
	       (setf params (plist-put params :file file))))))
    (with-temp-buffer (insert text)
      (shell-command-on-region (point-min) (point-max) command))
    (message "COMMAND: %S " command)))

(defun speech-play (file)
  (shell-command (format "play %S" file)))

(defvar *screenplay* nil)

(defvar *voices* nil)

(defvar *voice* nil)
(defvar *voice-key* nil)

(defmacro define-voice (name &rest args)
  `(push ',(cons name args) *voices*))

(defun voice-parameters (name &rest args)
  (rest (assoc name *voices*)))

(defmacro with-voice (voice &rest body)
  `(let ((*voice* (voice-parameters ,voice))
	 (*voice-key* ,voice))
     ,@body))

(defun say (key text)
  (let ((file (concat 
	       (substring (symbol-name key) 1)
	       ".wav")))
    (speech-render text *voice* file)
    ;;(speech-play file)
    (push (list key text file) *screenplay*)))

;;;;;;;;;;;;;;;;;;;;;

(setf *voices* nil)
(setf *voice* nil)
(setf *voice-key* nil)
(setf *screenplay* nil)

(define-voice :computer :pitch 12 :speed 110 :voice mb-en1)

(with-voice :computer
            (say :welcome "Welcome to dance ball!")
            (say :describe-arrow-keys "Use the arrow keys to move.")
            (say :describe-enter-key "Use the enter or shift keys to make a selection.")
            (say :describe-escape-key "Press the escape key to go back.")
            (say :good "Good!")
            (say :marvelous "Marvelous!")
            (say :excellent "Excellent!")
            (say :amazing "Amazing work!")
            (say :good-show "Good show!")
            (say :ouch "Ouch!")
            (say :not-good "This doesn't look good!")
            (say :keep-moving "Keep moving!")
            (say :get-ready "Are you ready?")
            (say :now-survive "Now, survive the song!")
            (say :got-star "You got the star!")
            (say :now-dance "Now, dance!")
            (say :please-connect "Please connect a game controller or dance pad to continue.")
            (say :now-press "Now press the controller button you wish to use for this command.")
            (say :now-lets-configure "Now let's configure your controller.")
            (say :press-any-button "To begin, press any button on the controller you want to use.")
            (say :player-1 "Player one.")
            (say :player-2 "Player two.")
            (say :easy "Easy difficulty.")
            (say :medium "Medium difficulty.")
            (say :hard "Hard difficulty.")
            (say :0 "Zero.")
            (say :1 "One.")
            (say :2 "Two.")
            (say :3 "Three.")
            (say :4 "Four.")
            (say :5 "Five.")
            (say :6 "Six.")
            (say :7 "Seven.")
            (say :8 "Eight.")
            (say :9 "Nine.")
            (say :beats-per-minute "Beats per minute")
            (say :save-your-configuration "Choose the save option from the menu to save your configuration.")
            (say :now-press-in-order "Now let's press each button in order.")
            (say :up "Press the up button.")
            (say :down "Press the down button.")
            (say :left "Press the left button.")
            (say :right "Press the right button.")
            (say :cross "Press the cross button.")
            (say :circle "Press the circle button.")
            (say :left-bumper "Press the left shoulder button.")
            (say :right-bumper "Press the right shoulder button.")
            (say :to-start-over "If you want to start over, press the escape key.")
            (say :failed "You failed!")
            (say :you-win "You win!")
            (say :finished "We're finished. Your controller is ready to use.")
            (say :basic-help-1 "Press the buttons on the beat or the half-beat to move your triangle!")
            (say :basic-help-2 "Press cross or circle (or the shift key) to fire the bouncing Dance ball!")
            (say :basic-help-3 "Use the Dance ball to break colored bricks and pick up their colors.")
            (say :basic-help-4 "Break through dashed barriers with the correct color ball.")
            (say :basic-help-5 "Use shields to defend against bullets.")
            (say :basic-help-6 "When arrows sweep across the screen, hold the opposite direction until it passes!")
            (say :basic-help-7 "Get to the Star and survive the song!")
            (say :basic-help-8 "Follow the sparkling black and white point to find the star.")
            (say :puzzle-mode "Puzzle play.")
            (say :challenge-mode "Challenge play.")
            (say :match-mode "Match three play.")
            (say :versus-mode "Versus play.")
            (say :mines "Avoid the mines!")
            (say :robots "Destroy the robots!")
            (say :ghosts "Destroy the ghosts!")
            (say :holds "Hold the indicated arrows!")
            (say :snakes "Avoid the snakes and their trails!"))

;; (with-temp-buffer 
;;   (insert (prin1-to-string *screenplay*)) 
;;   (write-file "~/danceball/screenplay.sexp"))

;; ;; ecasound -f:16,2,44100 -i resample,auto,bar96k.wav -o foo44100.wav

;; (dolist (file (mapcar #'third *screenplay*))
;;   (shell-command (format "ecasound -f:s16_le,1,44100 -i resample,auto,%s -x -o:_%s" file file) "*ecasound*"))

;; (dolist (file (mapcar #'third *screenplay*))
;;   (shell-command (delete-file file)))

;; (defun slime-trace-functions (&rest functions)
;;   (dolist (func functions)
;;     (message "%s" (slime-eval `(swank:swank-toggle-trace ,(slime-trace-query func))))))

;; (slime-trace-functions 'skyw0r::at-beginning-p 'skyw0r::at-end-p
;; 		       'skyw0r::restrict-point 'skyw0r::next-line
;; 		       'skyw0r::previous-line 'skyw0r::expand
;; 		       'skyw0r::unexpand 'skyw0r::toggle-expanded
;; 		       'skyw0r::activate 'skyw0r::change-leaf)


(provide 'voice)
;;; voice.el ends here


