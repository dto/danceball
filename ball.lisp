(in-package :danceball)

;;; The bouncing Danceball

(defclass ball (thing physical)
  ((max-dx :initform 100)
   (max-dy :initform 100)
   (max-ddx :initform 0.3)
   (max-ddy :initform 0.3)
   (image :initform "ball.png")
   (kick-clock :initform 0)
   (color :initform "white")
   (count :initform 0)
   (num-bounces :initform 0 :initarg :num-bounces :accessor num-bounces)
   (seeking-enemies-p :initform nil :accessor seeking-enemies-p)
   (fired-on-half-beat-p :initform nil :initarg :fired-on-half-beat-p :accessor fired-on-half-beat-p)
   (returning-p :initform nil :accessor returning-p :initarg returning-p)
   (last-collision :initform nil :accessor last-collision)))

;;; Make the ball a certain size

(defparameter *ball-size* (units 1))

(defmethod initialize-instance :after ((ball ball) &key)
  (set-ball ball)
  (resize ball *ball-size* *ball-size*))

;;; A basic bounce method

(defmethod bounced-enough ((ball ball))
  (> (num-bounces ball) 7))

(defmethod bounce ((ball ball) &optional (speed 10) collider)
  (restore-location ball)
  (reset-physics ball)
  (impel ball :speed speed :heading (next-heading ball)))

(defmethod bounce :around ((ball ball) &optional (speed 10) collider)
  (when collider (setf (last-collision ball) collider))
  (incf (num-bounces ball))
  (if (bounced-enough ball)
      (setf (returning-p ball) t)
      (setf (returning-p ball) nil))
  (call-next-method))
  
;;; Keeping the kicks spaced out

;; The KICK-CLOCK is a counter used to prevent the ball from being kicked
;; repeatedly in a short time.

(defparameter *kick-disabled-time* 15)

(defmethod disable-kicking ((ball ball))
  (setf (slot-value ball 'kick-clock) *kick-disabled-time*))

(defmethod recently-kicked-p ((ball ball))
  (plusp (slot-value ball 'kick-clock)))

;;; Maintain the KICK-CLOCK and keep the ball onscreen

(defmethod update ((ball ball))
  (with-slots (x y kick-clock heading speed color) ball
    (when (plusp kick-clock)
      (decf kick-clock))
    (restrict-to-buffer ball)))

(defmethod knock-toward-center ((ball ball))
  (grab (player-1))
  (destroy ball))

(defmethod collide ((ball ball) (wall wall))
  (bounce ball 10 wall))

(defmethod collide :around ((ball ball) (wall wall))
  (bounce ball 10 wall))

(defmethod update-heading :around ((ball ball))
  (if (returning-p ball)
      (setf (heading ball)
            (heading-to-cursor ball))
      (call-next-method)))

(defmethod movement-heading :around ((ball ball))
  (if (returning-p ball)
      (setf (heading ball)
            (heading-to-cursor ball))
      (call-next-method)))

(defmethod thrust-x ((ball ball))
  (when (returning-p ball) 2.0))

(defmethod thrust-y ((ball ball))
  (when (returning-p ball) 2.0))

;; (defmethod nearby-enemies ((ball ball))
;;   (multiple-value-bind (top left right bottom) (bounding-box ball)
;;     (let ((margin 380))
;;       (let ((t0 (- top margin))
;;             (l0 (- left margin))
;;             (r0 (+ right margin))
;;             (b0 (+ bottom margin)))
;;         (let (enemies)
;;           (quadtree-map-collisions (quadtree (arena))
;; 			           t0 l0 r0 b0
;; 			           #'(lambda (thing)
;;                                        (when (typep thing (find-class 'enemy))
;;                                          (message "found enemy")
;;                                          (push thing enemies))))
;;           enemies)))))

(defmethod find-enemy ((ball ball) &optional (range 450))
  (let ((enemies
          (loop for thing being the hash-values of (objects (current-buffer))
                when (and (typep (find-object thing) (find-class 'enemy))
                          (colliding-with-bounding-box-p
                           (find-object thing)
                           (cfloat (- (y ball) range))
                           (cfloat (- (x ball) range))
                           (cfloat (+ (x ball) range))
                           (cfloat (+ (y ball) range))))
                  collect (find-object thing))))
    enemies))

(defmethod find-enemy :around ((ball ball) &optional (range 100))
  (when (seeking-enemies-p ball)
    (call-next-method)))

(defmethod next-heading ((ball ball))
  (let ((enemies (find-enemy ball)))
    (if (> (length enemies) 1)
        (heading-between ball (random-choose (delete (last-collision ball)
                                                     enemies)))
        (heading-between ball (player-1)))))

(defmethod next-heading :around ((ball ball))
  (if (bounced-enough ball)
      (heading-between ball (player-1))
      (call-next-method)))

;;; Limit destruction per trip into end-zone

;; The Danceball can sometimes do too much damage to the enemy fortress
;; in one kick, bouncing and destroying many bricks. Here when defining
;; the BALL, BRICK collision we limit the number of bricks that can be
;; destroyed in one trip to the end-zone.

(defparameter *maximum-bricks-destroyed* 4)

(defmethod collide ((ball ball) (brick brick))
  (with-slots (count) ball
    (when (< count *maximum-bricks-destroyed*)
      (progn
        (paint ball (color brick))
        (destroy brick)
	(play-sound ball (random-choose *color-sounds*))
	(bounce ball 10 brick)
	(incf count)))))

(defparameter *bounceback-distance* 600)

;; Bounce back to player if ball gets too far
(defmethod update :after ((ball ball))
  (when (>= (distance-to-cursor ball)
            *bounceback-distance*)
    (bounce ball)))

(defmethod collide :around ((ball ball) (thing grid-actor))
  (if (half-beat-shielded-p thing)
      (if (fired-on-half-beat-p ball)
          (progn (display-message "HALF-BEAT HIT!")
                 (call-next-method))
          (progn (display-message "NO HIT")
                 (bounce ball 10 thing)))
      (call-next-method)))

(defmethod collide ((gate gate) (ball ball))
  (bounce ball)
  (when (string= (color gate) (color ball))
    (open-gate gate)
    (destroy gate)
    (play-sample (random-choose *wubble-sounds*))
    (percent-of-time 30
      (say (dj) (random-choose '(:good :marvelous :excellent :amazing :good-show))))
    (award-extra-points (dj) 2000)
    (destroy-all-onscreen-enemies)))

(defun destroy-all-onscreen-enemies ()
  (play-sample "clear.wav")
  (let ((enemies (find-instances (arena) 'robot-1))
        (bullets (find-instances (arena) 'bullet)))
    (multiple-value-bind (top left right bottom) (window-bounding-box (arena))
      (dolist (enemy (append enemies bullets))
        (with-slots (x y) enemy
          (when (and (<= left x right)
                     (<= top y bottom))
            (make-sparks (+ x (units 0.5))
                         (+ y (units 0.5))))
            (destroy enemy))))))

(defun destroy-all-objects (class)
  (let ((objects (find-instances (arena) class)))
      (dolist (object objects)
        (make-sparks (+ (x object) (units 0.5))
                     (+ (y object) (units 0.5)))
        (destroy object))))

;; ball has no resistance
(defmethod decay ((ball ball) value)
  value)

(defmethod collide ((ball ball) (enemy enemy))
  (damage enemy)
  (bounce ball 10 enemy)
  (repel ball enemy 5)
  (setf (seeking-enemies-p ball) t))

;;; Can't grab ball if it was just kicked, or is being carried

(defmethod collide ((ship ship) (ball ball))
  (unless (recently-kicked-p ball)
    (grab ship)))

(defmethod collide ((ball ball) (ship ship))
  (unless (recently-kicked-p ball)
    (grab ship)))

